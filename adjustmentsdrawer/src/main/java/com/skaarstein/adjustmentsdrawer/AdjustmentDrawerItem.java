package com.skaarstein.adjustmentsdrawer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by Martin on 17.04.2016.
 */
public class AdjustmentDrawerItem extends LinearLayout {

    public AdjustmentDrawerItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AdjustmentDrawerItem(Context context) {
        super(context);
    }

    public AdjustmentDrawerItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
