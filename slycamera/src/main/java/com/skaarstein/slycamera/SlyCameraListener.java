package com.skaarstein.slycamera;

/**
 * Created by Martin on 06.04.2016.
 */

import java.io.File;

/**
 * RotationListener for SlyCamera
 */
public interface SlyCameraListener{

    /**
     * Called when phone orientation changes
     * @param orientation constant representing camera orientation
     */
    void onCameraRotated(int orientation);
    /**
     * Called when camera cache dir is cleared
     */
    void onCameraCacheCleared(long sizeCleared, int nrOfFilesRemoved, boolean forcedClear);
    /**
     * Called on focus unlocked
     */
    void onUnlockFocus();

    /**
     * Called on focus lock
     */
    void onLockFocus();
    /**
     * Called on precapture
     */
    void onPreCapture();
    /**
     * Called when an image capture is complite and image
     * ready.
     * @param imageFile temp image file in catch register;
     */
    void onCaptureComplete(File imageFile);

    /**
     * Called on error
     * @param code the error code
     * @param msg the error message
     */
    void onError(int code, String msg);
}
