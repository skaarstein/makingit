package com.skaarstein.slycamera;

/**
 * Created by Martin on 04.04.2016.
 */
public interface SlyCameraControls {
    boolean isFrontCameraSupported();
    boolean isFlashSupported();
    void setFlashType(SlyCamera.FlashType type);
    SlyCamera.FlashType getFlashType();
    void setFocusMode(SlyCamera.FocusMode mode);
    SlyCamera.FocusMode getFocusMode();
    void setExposureTime(int time);
    int getExposureTime();
    void setISO(int iso);
    int getISO();
    void setFrameDuration(int duration);
    int getFrameDuration();
    void setWhiteBalance(int balance);
    int getWhiteBalance();
    void setCameraMode(SlyCamera.CameraMode mode);
    SlyCamera.CameraMode getCameraMode();
    void takePicture();
    boolean setCameraType(SlyCamera.CameraType type);
    SlyCamera.CameraType getCameraType();
    void setSaveSettingsOnClose(boolean save);
    boolean getSaveSettingsOnClose();
    void clearCameraCatch(boolean forcedClear);
    void setCameraCatchSize(int size);
    int getCameraCatchSize();
    void addSlyCameraListener(SlyCameraListener listener);
    void removeSlyCameraListener(SlyCameraListener listener);
    int getCurrentOrientation();
    int getUsedCacheSize();
}
