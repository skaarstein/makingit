
package com.skaarstein.slycamera;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;

import com.skaarstein.slycamera.utils.RotationListener;
import com.skaarstein.slycamera.utils.RotationManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * A {@link TextureView} that can be adjusted to a specified aspect ratio.
 */
public class SlyCamera extends TextureView implements TextureView.SurfaceTextureListener, SlyCameraControls{

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    private static final String TAG = SlyCamera.class.getName();

    public SlyCamera(Context context) {
        this(context, null);
        init(context);
    }

    public SlyCamera(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context);
    }

    public SlyCamera(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context){
        mCameraListeners = new ArrayList<>();
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    private void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        Log.d(TAG, width + "x" + height + " is aspect ratio");
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        Log.d(TAG, width + "," + height + " is measured dimension");
        Log.d(TAG, "Aspect ratio: " + (float)mRatioHeight / mRatioWidth + ", " + (float)mRatioWidth / mRatioHeight);
        Log.d(TAG, "Aspect ratio relative: " + (float)height / mRatioWidth + ", " + (float)width / mRatioHeight);
        Log.d(TAG, "Aspect ratio relative revert: " + (float) mRatioWidth / width + ", " + (float) mRatioHeight / height);

        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(width * mRatioHeight / mRatioWidth, height);
                Log.d(TAG, "Measured dimension min is: " + width * mRatioHeight / mRatioWidth + ", " + height);
            } else {
                setMeasuredDimension(width, height);
                Log.d(TAG, "Measured dimension equal is: " + width + ", " + height);
            }
        }
    }


    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final SparseArray<String> CAMERAS = new SparseArray<>();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    public enum FlashType{
        FLASH_AUTO(0),
        FLASH_OFF(1),
        FLASH_ON(2);

        private final int value;

        FlashType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum FocusMode{
        AUTO_FOCUS(0),
        MANUAL_FOCUS(1);

        private final int value;

        FocusMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum CameraMode{
        SINGLE_SHOT(0),
        BURST_MODE(1);

        private final int value;

        CameraMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum CameraType{
        CAMERA_FRONT(0),
        CAMERA_BACK(1);

        private final int value;

        CameraType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    }

    /**
     * CameraActivity state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * CameraActivity state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * CameraActivity state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * CameraActivity state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * CameraActivity state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;

    /**
     * Max preview width that is guaranteed by CameraFragment API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by CameraFragment API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;


    /**
     * The current selected FlashType
     */
    private static FlashType mFlashType;

    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #mPreviewRequestBuilder}
     */
    private CaptureRequest mPreviewRequest;

    /**
     * The current state of camera state for taking pictures.
     *
     * @see #mCaptureCallback
     */
    private int mState = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * Whether the current camera device supports Flash or not.
     */
    private boolean mFlashSupported;

    /**
     * Whether the divice got a front facing camera
     */
    private boolean mFrontCameraSupported;

    /**
     * Returns true if cameras has been initialized
     */
    private boolean mCamerasLoaded;

    /**
     * Holds the current camera type
     */
    private CameraType mCameraType;

    /**
     * ID of the current {@link CameraDevice}.
     */
    private String mCameraId;

    /**
     * A {@link CameraCaptureSession } for camera preview.
     */
    private CameraCaptureSession mCaptureSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * The {@link Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader mImageReader;

    /**
     * An activity for camera requests
     */
    private Activity activity;

    /**
     * RotationListener for camera events
     */
    private ArrayList<SlyCameraListener> mCameraListeners;

    /**
     * Selected focus mode for camera
     */
    private FocusMode mFocusMode;

    /**
     * Camera exposure time
     */
    private int mExposureTime;

    /**
     * Camera iso
     */
    private int mISO;

    /**
     * Camera frame duration
     */
    private int mFrameDuration;

    /**
     * Camera white balance
     */
    private int mWhiteBalance;

    /**
     * The current camera mode
     */
    private CameraMode mCameraMode;

    /**
     * The total size available for temp images
     */
    private int mCameraCacheSize;

    /**
     * True if settings are to be stored in shared preferences.
     */
    private boolean mSaveSettings = true;

    /**
     * Listens for rotation changes
     */
    private RotationManager mRotationManager;

    /**
     * The current camera rotation;
     */
    private int mCurrentRotation;

    
    private void onCameraCacheCleared(long sizeCleared, int nrOfFilesRemoved, boolean forcedClear) {
        for(SlyCameraListener l: mCameraListeners){
            l.onCameraCacheCleared(sizeCleared, nrOfFilesRemoved, forcedClear);
        }
    }

    
    private void onUnlockFocus() {
        for(SlyCameraListener l: mCameraListeners){
            l.onUnlockFocus();
        }
    }

    
    private void onLockFocus() {
        for(SlyCameraListener l: mCameraListeners){
            l.onLockFocus();
        }
    }

    
    private void onPreCapture() {
        for(SlyCameraListener l: mCameraListeners){
            l.onPreCapture();
        }
    }

    
    private void onCaptureComplete(File imageFile) {
        for(SlyCameraListener l: mCameraListeners){
            l.onCaptureComplete(imageFile);
        }
    }

    
    private void onError(int code, String msg) {
        for(SlyCameraListener l: mCameraListeners){
            l.onError(code, msg);
        }
    }

    private void onCameraRotated(int orientation){
        mCurrentRotation = orientation;
        for(SlyCameraListener l: mCameraListeners){
            l.onCameraRotated(orientation);
        }
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
        openCamera(width, height, CAMERAS.get(mCameraType.getValue()));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
        configureTransform(width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture texture) {
    }

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

    };

    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(final ImageReader reader) {

            mBackgroundHandler.post(new Runnable() {
                @Override
                public void run() {

                    Image lastCapture = reader.acquireLatestImage();
                    ByteBuffer buffer = lastCapture.getPlanes()[0].getBuffer();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);

                    FileOutputStream output = null;
                    File outputFile = null;

                    try {
                        File cameraDir = new File(activity.getCacheDir().getAbsolutePath()
                                + "/" + TAG);

                        if(!cameraDir.exists()){
                            cameraDir.mkdirs();
                        }

                        int name = cameraDir.list().length;
                        outputFile = File.createTempFile("cam_temp-" + name, "jpg", cameraDir);

                        output = new FileOutputStream(outputFile);
                        output.write(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        lastCapture.close();
                        Log.d(TAG, "Image captured");
                        final File displayFile = outputFile;
                        try {
                            output.close();
                        } catch (IOException e) {
                            Log.e(TAG, e.getMessage());
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onCaptureComplete(displayFile);
                            }
                        });
                    }
                }
            });
        }

    };



    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    onUnlockFocus();
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            captureStillPicture();
                        } else {
                            runPreCaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    onLockFocus();
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            Size size = Collections.min(bigEnough, new CompareSizesByArea());
            return size;
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    private static Size findRelativeSize(Size[] choices, Size toMatch) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = toMatch.getWidth();
        int h = toMatch.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= w && option.getHeight() <= h){
                bigEnough.add(option);
            } else {
                notBigEnough.add(option);
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            Size size = Collections.max(bigEnough, new CompareSizesByArea());
            return size;
        } else if (notBigEnough.size() > 0) {
            return Collections.min(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    /**
     * Itterates trough avaliable cameras
     */
    private void findAvaliableCameras(){
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null) {
                    switch (facing) {
                        case CameraCharacteristics.LENS_FACING_FRONT: {
                            CAMERAS.append(CameraType.CAMERA_FRONT.getValue(), cameraId);
                            mFrontCameraSupported = true;
                            break;
                        }
                        case CameraCharacteristics.LENS_FACING_BACK: {
                            CAMERAS.append(CameraType.CAMERA_BACK.getValue(), cameraId);
                            break;
                        }
                    }
                }
            }
        }catch (CameraAccessException e) {
            Log.e(TAG, e.getMessage());
        }
    }
    /**
     * Sets up member variables related to camera.
     */
    private void setUpCameraOutputs(int width, int height, String cameraId) {
        if (!mCamerasLoaded)return;

        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics
                    = manager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            if (map == null) {
                return;
            }

            Size resolution = findRelativeSize(map.getOutputSizes(ImageFormat.JPEG),
                    new Size(1920, 1080));

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            int sensorOrientation =
                    characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            boolean swappedDimensions = false;

            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (sensorOrientation == 90 || sensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (sensorOrientation == 0 || sensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e(TAG, "Display rotation is invalid: " + displayRotation);
            }

            Point displaySize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(displaySize);

            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
                Log.d(TAG, "Swapped: " + width + "x" + height + ", "
                        + maxPreviewWidth + "x" + maxPreviewHeight);
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
            // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
            // garbage capture data.
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, Collections.max(
                            Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                            new CompareSizesByArea()));

            mImageReader = ImageReader.newInstance(
                    resolution.getWidth(),
                    resolution.getHeight(),
                    ImageFormat.JPEG, /*maxImages*/2);

            Log.d(TAG, "Image resolution: " + resolution.getWidth() +"x" + resolution.getHeight());
            Log.d(TAG, "Preview size: " + mPreviewSize.getHeight() + "x" + mPreviewSize.getWidth());

            mImageReader.setOnImageAvailableListener(
                    mOnImageAvailableListener, mBackgroundHandler);

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setAspectRatio(
                        mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                setAspectRatio(
                        mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }

            // Check if the flash is supported.
            Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
            mFlashSupported = available == null ? false : available;

            mCameraId = cameraId;
            return;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();

        }
    }

    /**
     * Opens a camera with a given id
     * @param cameraID id of camera to load
     */
    private void openCamera(int width, int height, String cameraID) {
        if (!mCamerasLoaded)return;

        setUpCameraOutputs(width, height, cameraID);
        configureTransform(width, height);

        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);

        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        } catch (SecurityException e){
            e.printStackTrace();
        }
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            final SurfaceTexture texture = this.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                //setFlashMode(mPreviewRequestBuilder);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {

                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `cameraView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `cameraView` is fixed.
     *
     * @param viewWidth  The width of `cameraView`
     * @param viewHeight The height of `cameraView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {

        if (null == mPreviewSize) {
            return;
        }

        Log.d(TAG, "Dimension: " + viewWidth + "x" + viewHeight);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        float scale = Math.max((float) viewHeight / mPreviewSize.getHeight(),
                (float) viewWidth / mPreviewSize.getWidth());

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
            Log.d(TAG, "Scale: " + scale);
        } else {
            scale = Math.max((float) viewHeight / mPreviewSize.getWidth(),
                    (float) viewWidth / mPreviewSize.getHeight());
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            matrix.postScale(scale, scale, centerX, centerY);

            Log.d(TAG, "Scale: " + scale);

            if (Surface.ROTATION_180 == rotation) {
                matrix.postRotate(180, centerX, centerY);
                Log.d(TAG, "Scale ro 180: " + scale);
            }
        }
        this.setTransform(matrix);
    }



    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            captureStillPicture();
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPreCaptureSequence() {
        try {
            onPreCapture();
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {
        try{
            if (null == mCameraDevice) {
                return;
            }

            mState = STATE_PICTURE_TAKEN;
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            setFlashMode(captureBuilder);

            // Orientation
            //int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(mCurrentRotation));

            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    unlockFocus();
                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), CaptureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setFlashMode(CaptureRequest.Builder requestBuilder) {
        if (mFlashSupported) {
            switch (mFlashType){
                case FLASH_AUTO:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                    break;
                }
                case FLASH_OFF:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON);
                    break;
                }
                case FLASH_ON:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                    break;
                }
            }

        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            setFlashMode(mPreviewRequestBuilder);
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                    mBackgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void manualFocus(){

    }

    private void checkCache(){
        mBackgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                File cacheDir = new File(activity.getCacheDir().getAbsolutePath()
                        + "/" + TAG);
                if(cacheDir.exists()){
                    long sizeCleared = 0;
                    long size = cacheDir.length();
                    int count = 0;

                    for(File f : cacheDir.listFiles()){
                        size -= f.length();
                        sizeCleared += f.length();
                        count += 1;
                        f.delete();

                        if(size < mCameraCacheSize){
                            return;
                        }
                    }

                    final long clearedSize = sizeCleared;
                    final int finalCount = count;

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onCameraCacheCleared(clearedSize, finalCount, false);
                        }
                    });
                }
            }
        });
    }

    private static final String CAMERA_PREFERENCES = "camera_preferences";
    private static final String PREF_SAVE_SETTINGS = "save_settings";
    private static final String PREF_CAMERA_TYPE = "camera_type";
    private static final String PREF_FLASH_TYPE = "flash_type";
    private static final String PREF_FOCUS_MODE = "focus_mode";
    private static final String PREF_EXPOSURE_TIME = "exposure_time";
    private static final String PREF_ISO = "iso";
    private static final String PREF_FRAME_DURATION = "frame_duration";
    private static final String PREF_WHITE_BALANCE = "white_balance";
    private static final String PREF_CAMERA_MODE = "camera_mode";
    private static final String PREF_CACHE_SIZE = "cache_size";

    private void saveSettings(){
        SharedPreferences.Editor editor = activity
                .getSharedPreferences(CAMERA_PREFERENCES, Context.MODE_PRIVATE).edit();

        editor.putBoolean(PREF_SAVE_SETTINGS, mSaveSettings);

        if(mSaveSettings){
            editor.putInt(PREF_CAMERA_TYPE, mCameraType.getValue());
            editor.putInt(PREF_FLASH_TYPE, mFlashType.getValue());
            editor.putInt(PREF_FOCUS_MODE, mFocusMode.getValue());
            editor.putInt(PREF_EXPOSURE_TIME, mExposureTime);
            editor.putInt(PREF_ISO, mISO);
            editor.putInt(PREF_FRAME_DURATION, mFrameDuration);
            editor.putInt(PREF_WHITE_BALANCE, mWhiteBalance);
            editor.putInt(PREF_CAMERA_MODE, mCameraMode.getValue());
            editor.putInt(PREF_CACHE_SIZE, mCameraCacheSize);
        }

        editor.commit();
    }

    private void loadSettings(){
        SharedPreferences pref = activity
                .getSharedPreferences(CAMERA_PREFERENCES, Context.MODE_PRIVATE);

        mSaveSettings = pref.getBoolean(PREF_SAVE_SETTINGS, true);
        mCameraType = CameraType.values()[pref.getInt(PREF_CAMERA_TYPE,
                CameraType.CAMERA_BACK.getValue())];
        mFlashType = FlashType.values()[pref.getInt(PREF_FLASH_TYPE,
                FlashType.FLASH_AUTO.getValue())];
        mFocusMode = FocusMode.values()[pref.getInt(PREF_FOCUS_MODE,
                FocusMode.AUTO_FOCUS.getValue())];
        mExposureTime = pref.getInt(PREF_EXPOSURE_TIME, 0);
        mISO = pref.getInt(PREF_ISO, 0);
        mFrameDuration = pref.getInt(PREF_FRAME_DURATION, 0);
        mWhiteBalance = pref.getInt(PREF_WHITE_BALANCE, 0);
        mCameraMode = CameraMode.values()[pref.getInt(PREF_CAMERA_MODE,
                CameraMode.SINGLE_SHOT.getValue())];
        mCameraCacheSize = pref.getInt(PREF_CACHE_SIZE, 100000000);
    }

    private RotationListener mRotationListener = new RotationListener() {
        private float rotThreshold = 2.5f;

        @Override
        public void onRotation(float rotationX, float rotationY, float rotationZ) {
            if(rotationZ <= rotThreshold && rotationZ >= -rotThreshold){
                onCameraRotated(Surface.ROTATION_0);
                return;
            }
            if(rotationZ <= 90 + rotThreshold && rotationZ >= 90 - rotThreshold){
                onCameraRotated(Surface.ROTATION_90);
                return;
            }
            if(rotationZ <= 180 + rotThreshold && rotationZ >= 180 - rotThreshold){
                onCameraRotated(Surface.ROTATION_180);
                return;
            }
            if(rotationZ <= (-90) + rotThreshold && rotationZ >= (-90) - rotThreshold){
                onCameraRotated(Surface.ROTATION_270);
                return;
            }
        }
    };

    /**
     * Change camera using camera type.
     * @param camera camera type
     * @return true if camera setup was sucessfull.
     */
    @Override
    public boolean setCameraType(CameraType camera){
        switch (camera){
            case CAMERA_BACK:{
                closeCamera();
                openCamera(this.getWidth(), this.getHeight(),
                        CAMERAS.get(CameraType.CAMERA_BACK.getValue()));
                mCameraType = CameraType.CAMERA_BACK;
                return true;
            }
            case CAMERA_FRONT:{
                closeCamera();
                openCamera(this.getWidth(), this.getHeight(),
                        CAMERAS.get(CameraType.CAMERA_FRONT.getValue()));
                mCameraType = CameraType.CAMERA_FRONT;
                return true;
            }
            default: return false;
        }
    }

    public CameraType getCameraType(){
        return mCameraType;
    }

    @Override
    public void setSaveSettingsOnClose(boolean save) {
        mSaveSettings = save;
    }

    @Override
    public boolean getSaveSettingsOnClose() {
        return mSaveSettings;
    }

    @Override
    public boolean isFrontCameraSupported(){
        return mFrontCameraSupported;
    }
    @Override
    public boolean isFlashSupported(){
        return mFlashSupported;
    }
    @Override
    public void setFlashType(FlashType type){
        mFlashType = type;
    }
    @Override
    public FlashType getFlashType(){
        return mFlashType;
    }

    @Override
    public int getUsedCacheSize() {
        File cacheDir = new File(activity.getCacheDir().getAbsolutePath()
                + "/" + TAG);
        if(cacheDir.exists()){
            return (int)cacheDir.length();
        }
        return 0;
    }

    /**
     * Initializes camera if avaliable
     * @param activity
     */
    public void startCamera(@NonNull Activity activity){
        startBackgroundThread();

        this.activity = activity;

        Log.d(TAG, "SlyCameraActivity started");

        if(!mCamerasLoaded){
            findAvaliableCameras();
            loadSettings();

            mRotationManager = new RotationManager(
                    (SensorManager) activity.getSystemService(Activity.SENSOR_SERVICE),
                    activity.getWindow().getWindowManager());

            mRotationManager.addRotationListener(mRotationListener);

            mCamerasLoaded = true;
        }

        if(mRotationManager != null){
            mRotationManager.startListening();
        }

        if (this.isAvailable()) {
            openCamera(this.getWidth(), this.getHeight(), CAMERAS.get(mCameraType.getValue()));
        } else {
            this.setSurfaceTextureListener(this);
        }
    }

    /**
     * Stops the running camera
     */
    public void stopCamera(){
        mRotationManager.stopListening();
        saveSettings();
        closeCamera();
        stopBackgroundThread();
    }
    @Override
    public void setFocusMode(FocusMode focusMode){
        this.mFocusMode = focusMode;
    }
    @Override
    public FocusMode getFocusMode(){
        return mFocusMode;
    }
    @Override
    public void setExposureTime(int time){
        this.mExposureTime = time;
    }
    @Override
    public int getExposureTime(){
        return mExposureTime;
    }
    @Override
    public void setISO(int iso){
        this.mISO = iso;
    }
    @Override
    public int getISO(){
        return mISO;
    }
    @Override
    public void setFrameDuration(int duration){
        this.mFrameDuration = duration;
    }
    @Override
    public int getFrameDuration(){
        return mFrameDuration;
    }
    @Override
    public void setWhiteBalance(int balance){
        this.mWhiteBalance = balance;
    }
    @Override
    public int getWhiteBalance(){
        return mWhiteBalance;
    }
    @Override
    public void setCameraMode(CameraMode mode){
        this.mCameraMode = mode;
    }
    @Override
    public CameraMode getCameraMode(){
        return mCameraMode;
    }

    @Override
    public void clearCameraCatch(final boolean forcedClear) {
        mBackgroundHandler.post(new Runnable() {
            @Override
            public void run() {

                long size = 0;
                int files = 0;

                File cacheDir = new File(activity.getCacheDir().getAbsolutePath()
                        + "/" + TAG);

                if(cacheDir.exists()){
                    for(File f : cacheDir.listFiles()){
                        size += f.length();
                        files += 1;
                        f.delete();
                    }
                }

                final long finalSize = size;
                final int finalCount = files;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onCameraCacheCleared(finalSize, finalCount, forcedClear);
                    }
                });
            }
        });
    }

    @Override
    public void setCameraCatchSize(int size) {
        mCameraCacheSize = size;
    }

    @Override
    public int getCameraCatchSize() {
        return mCameraCacheSize;
    }

    public SlyCameraControls getCameraControls(){
        return this;
    }
    /**
     * Initiate a still image capture.
     */
    @Override
    public void takePicture() {
        checkCache();
        lockFocus();
    }

    @Override
    public void addSlyCameraListener(SlyCameraListener listener) {
        mCameraListeners.add(listener);
    }

    @Override
    public void removeSlyCameraListener(SlyCameraListener listener) {
        mCameraListeners.remove(listener);
    }

    @Override
    public int getCurrentOrientation() {
        return mCurrentRotation;
    }
}
