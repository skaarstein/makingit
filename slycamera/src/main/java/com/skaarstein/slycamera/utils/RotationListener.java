package com.skaarstein.slycamera.utils;

/**
 * Created by Martin on 07.04.2016.
 */
public interface RotationListener {
    void onRotation(float rotationX, float rotationY, float rotationZ);
}
