package com.skaarstein.slycamera.utils;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import java.util.ArrayList;

/**
 * Created by Martin on 07.04.2016.
 */


public class RotationManager implements SensorEventListener {

    private static final String TAG = RotationManager.class.getName();

    private static final int SENSOR_DELAY_MICROS = 50 * 1000; // 50ms

    private final SensorManager mSensorManager;
    private final Sensor mRotationSensor;
    private final WindowManager mWindowManager;

    private int mLastAccuracy;
    private ArrayList<RotationListener> mListeners;

    private boolean isSensorRegistered;
    private boolean isSensorRunning;

    public RotationManager(SensorManager sensorManager, WindowManager windowManager) {
        mSensorManager = sensorManager;
        mWindowManager = windowManager;
        mListeners = new ArrayList<>();

        // Can be null if the sensor hardware is not available
        mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if(mRotationSensor == null) Log.e(TAG, "Unable to find sensor");
    }

    public void addRotationListener(@NonNull RotationListener listener){
        mListeners.add(listener);

        if(isSensorRunning && !isSensorRegistered){
            startListening();
        }
    }

    public boolean removeRotationListener(@NonNull RotationListener listener){
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
            return true;
        }
        return false;
    }

    public void removeAllRotationListeners(){
        mListeners = null;
    }

    public boolean isListenerRegistered(){
        if(mListeners.size() > 0) return true;
        return false;
    }

    public boolean isListeningForRotation(){
        return isSensorRegistered;
    }

    public void startListening() {
        if (mRotationSensor == null) {
            Log.e(TAG, "Rotation vector sensor not available; will not provide orientation data.");
            return;
        }
        if(isListenerRegistered()) {
            mSensorManager.registerListener(this, mRotationSensor, SENSOR_DELAY_MICROS);
            isSensorRegistered = true;
        }

        isSensorRunning = true;
    }

    public void stopListening() {
        if(isListeningForRotation()) {
            mSensorManager.unregisterListener(this);
            isSensorRegistered = false;
        }

        isSensorRunning = false;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (mLastAccuracy != accuracy) {
            mLastAccuracy = accuracy;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mListeners == null) {
            return;
        }
        if (mLastAccuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            return;
        }
        if (event.sensor == mRotationSensor) {
            updateOrientation(event.values);
        }
    }

    private void updateOrientation(float[] rotationVector) {
        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationVector);

        // By default, remap the axes as if the front of the
        // device screen was the instrument panel.
        int worldAxisForDeviceAxisX = SensorManager.AXIS_X;
        int worldAxisForDeviceAxisY = SensorManager.AXIS_Z;

        // Adjust the rotation matrix for the device orientation
        int screenRotation = mWindowManager.getDefaultDisplay().getRotation();
        if (screenRotation == Surface.ROTATION_0) {
            worldAxisForDeviceAxisX = SensorManager.AXIS_X;
            worldAxisForDeviceAxisY = SensorManager.AXIS_Z;
        } else if (screenRotation == Surface.ROTATION_90) {
            worldAxisForDeviceAxisX = SensorManager.AXIS_Z;
            worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_X;
        } else if (screenRotation == Surface.ROTATION_180) {
            worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_X;
            worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_Z;
        } else if (screenRotation == Surface.ROTATION_270) {
            worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_Z;
            worldAxisForDeviceAxisY = SensorManager.AXIS_X;
        }

        float[] adjustedRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisForDeviceAxisX,
                worldAxisForDeviceAxisY, adjustedRotationMatrix);

        float[] orientation = new float[3];
        SensorManager.getOrientation(adjustedRotationMatrix, orientation);

        // Convert radians to degrees
        float rotationX = -57.35f * orientation[0];
        float rotationY = -57.35f * orientation[1];
        float rotationZ = -57.35f * orientation[2];

        for(RotationListener r : mListeners){
            r.onRotation(rotationX, rotationY, rotationZ);
        }
    }
}