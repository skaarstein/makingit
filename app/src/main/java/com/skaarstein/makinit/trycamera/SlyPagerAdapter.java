package com.skaarstein.makinit.trycamera;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skaarstein.makinit.trycamera.PagedCameraView.*;
import com.skaarstein.slycamera.SlyCameraControls;

/**
 * Created by SlyOtis on 12.04.2016.
 */
public class SlyPagerAdapter extends PagerAdapter {

    public static final String TAG = SlyPagerAdapter.class.getName();
    private PagerControls pager;
    private SlyCameraControls controls;
    private Activity activity;

    public interface PagerControls{
        void setCurrentItem(int index, boolean smooth);
        void close();
    }

    public SlyPagerAdapter(Activity activity, PagerControls pager,
                           SlyCameraControls controls) {
        this.pager = pager;
        this.activity = activity;
        this.controls = controls;

    }

    public SlyCameraControls getCameraControls(){
        return controls;
    }

    public PagerControls getPageControls(){
        return pager;
    }

    public Activity getActivity(){
        return activity;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        switch (position){
            case 1: {
                View v = getSettingsView(container, inflater);
                container.addView(v);
                return v;
            }
            case 0:{
                View v = getCameraView(container, inflater);
                container.addView(v);
                return v;
            }
        }

        Log.d(TAG, "No view found");
        return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if(object instanceof View)
            container.removeView((View)object);
    }


    public View getCameraView(ViewGroup container, LayoutInflater inflater){
        return null;
    }

    public View getSettingsView(ViewGroup container, LayoutInflater inflater){
        return null;
    }
}
