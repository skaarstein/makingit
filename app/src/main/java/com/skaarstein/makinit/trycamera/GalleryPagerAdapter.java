package com.skaarstein.makinit.trycamera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

/**
 * Created by Martin on 13.04.2016.
 */
public class GalleryPagerAdapter extends PagerAdapter{

    private Activity activity;
    private Integer[] ids;
    private DatabaseHandler dh;
    private int currPosition;

    public GalleryPagerAdapter(Activity activity, @NonNull Integer[] ids) {
        this.activity = activity;
        this.ids = ids;
        this.dh = new DatabaseHandler(activity);
    }

    public int getCurrentPosition(){
        return currPosition;
    }

    public void updateImages(Integer[] objects){
        if(objects != null) this.ids = objects;
    }

    public int getImageId(int position){
        if(position < 0) return ids[ids.length -1];
        return ids[position];
    }

    public int getImagePosition(int id){
        for(int i = 0; i < ids.length; i++){
            if(ids[i] == id) return i;
        }
        return ids.length -1 ;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if(object instanceof View){
            container.removeView((View)object);
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView v = (ImageView)LayoutInflater.from(activity)
                .inflate(R.layout.camera_overlay_item, container, false);

        byte[] data = dh.getImageData(ids[position]);
        Bitmap tmp = ImageUtils.scaleImage(data, ImageResolution.P1080p);
        v.setImageBitmap(tmp);

        currPosition = position;
        container.addView(v);

        return v;
    }

    @Override
    public int getCount() {
        return ids.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
