package com.skaarstein.makinit.trycamera;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.services.ImageSaveIntent;
import com.skaarstein.makinit.slycamera.DisplayFragment;
import com.skaarstein.makinit.slycamera.SettingsFragment;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.DatabaseListener;
import com.skaarstein.makinit.views.AdjustmentDrawer;
import com.skaarstein.slycamera.SlyCamera;
import com.skaarstein.slycamera.SlyCameraControls;
import com.skaarstein.slycamera.SlyCameraListener;

import java.io.File;

/**
 * Created by Martin on 03.04.2016.
 */
public class PagedCameraView extends SlyPagerAdapter{

    private int ID;

    public PagedCameraView(Activity activity, int ID, PagerControls pager, SlyCameraControls controls) {
        super(activity, pager, controls);
        this.ID = ID;

        LocalBroadcastManager.getInstance(activity).registerReceiver(cameraReceiver,
                new IntentFilter(ImageSaveIntent.BROADCAST_ACTION));
    }

    //Settings view;
    public static final String PREF_NAME_SETTINGS = "camera_settings";
    public static final String PREF_CLEAR_CACHE_ON_CLOSE = "clear_on_close";
    public static final String PREF_SAVE_TO_GALLERY = "save_to_gallery";
    public static final String PREF_PROMPT_TO_SAVE = "clear_on_close";
    public static final String PREF_OVERLAY_OPACITY = "overlay_opacity";
    public static final String PREF_OVERLAY_VISIBILITY = "overlay_visible";
    public static final String PREF_OVERLAY_ID = "clear_on_close";
    public static final String PREF_SAVE_ON_CLOSE = "save_on_close";
    public static final String PREF_CACHE_SIZE = "cache_size";

    private Switch autoClearCache;
    private Switch saveToGallery;
    private Switch autoSaveSettings;
    private Switch promptToSave;
    private TextView cacheSizeValue;
    private SeekBar cacheSize;
    private TextView overlayOpacityValue;
    private SeekBar overlayOpacity;

    private boolean mAutoClearCatche;
    private boolean mSaveToGallery;
    private boolean mPromptToSave;
    private boolean mAutoSaveSettings;
    private int mCatchSize;

    //Camera view;
    private int mOverlayID;
    private int mOverlayOpacity;
    private GalleryPagerAdapter galleryAdapter;

    private ViewPager overlay;
    private ImageButton overlayToggle;
    private ImageButton flashToggle;
    private ImageButton switchToggle;
    private ImageButton settings;
    private ImageButton adjustments;
    private ImageButton close;
    private ImageButton capture;
    private AdjustmentDrawer drawer;

    private boolean mOverlayVisible;
    private boolean mInitialized;

    private int mLastDegree = 0;
    private int mCurrDegree = 0;
    int mRotationDuration = 300;

    @Override
    public View getSettingsView(ViewGroup container, LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_camera_settings, container, false);
        view.findViewById(R.id.cs_open_camera).setOnClickListener(onClickListener);
        view.findViewById(R.id.cs_close_camera).setOnClickListener(onClickListener);
        view.findViewById(R.id.cs_clear_cache).setOnClickListener(onClickListener);

        autoClearCache = (Switch) view.findViewById(R.id.cs_auto_clear_cache);
        saveToGallery = (Switch) view.findViewById(R.id.cs_save_to_gallery);
        autoSaveSettings = (Switch) view.findViewById(R.id.cs_auto_save_settings);
        promptToSave = (Switch) view.findViewById(R.id.cs_prompt_to_save);
        cacheSize = (SeekBar) view.findViewById(R.id.cs_catch_size);
        cacheSizeValue = (TextView) view.findViewById(R.id.cs_catch_size_value);
        overlayOpacity = (SeekBar)view.findViewById(R.id.cs_overlay_opacity);
        overlayOpacityValue = (TextView)view.findViewById(R.id.cs_overlay_opacity_value);

        overlayOpacity.setOnSeekBarChangeListener(seekBarChangeListener);
        cacheSize.setOnSeekBarChangeListener(seekBarChangeListener);
        updateOpacity(mOverlayOpacity);
        autoClearCache.setChecked(mAutoClearCatche);
        saveToGallery.setChecked(mSaveToGallery);
        autoSaveSettings.setChecked(mAutoSaveSettings);
        promptToSave.setChecked(mPromptToSave);
        cacheSize.setProgress(mCatchSize);
        cacheSizeValue.setText(mCatchSize + " MB");

        return view;
    }

    @Override
    public View getCameraView(ViewGroup container, LayoutInflater inflater){
        View view = inflater.inflate(R.layout.fragment_sly_camera, container, false);
        settings =(ImageButton)view.findViewById(R.id.camera_settings);
        settings.setOnClickListener(onClickListener);
        adjustments = (ImageButton)view.findViewById(R.id.camera_adjustments);
        adjustments.setOnClickListener(onClickListener);
        close = (ImageButton)view.findViewById(R.id.camera_close);
        close.setOnClickListener(onClickListener);
        capture = (ImageButton)view.findViewById(R.id.camera_picture);
        capture.setOnClickListener(onClickListener);
        overlay = (ViewPager)view.findViewById(R.id.screen_overlay);
        overlayToggle = (ImageButton)view.findViewById(R.id.camera_overlay);
        overlayToggle.setOnClickListener(onClickListener);
        flashToggle = (ImageButton)view.findViewById(R.id.camera_flash);
        flashToggle.setOnClickListener(onClickListener);
        switchToggle = (ImageButton)view.findViewById(R.id.camera_switch);
        switchToggle.setOnClickListener(onClickListener);
        drawer = (AdjustmentDrawer)view.findViewById(R.id.adjustments_drawer);
        drawer.setAdjustmentDrawerListener(adjustmentDrawerListener);
        drawer.setDuration(200, 300, 300);
        drawer.setInterpolator(new BounceInterpolator(),
                new DecelerateInterpolator(), new DecelerateInterpolator());
        drawer.setDrawerMargin(getActivity().getResources().getDimension(R.dimen.camera_btn_large));

        try {
            drawer.addItem(AdjustmentDrawer.DrawerItem.EDIT);
            drawer.addItem(AdjustmentDrawer.DrawerItem.OVERLAY_OPACITY);
            drawer.addItem(AdjustmentDrawer.DrawerItem.EXPOSURE);
        }catch (AdjustmentDrawer.AdjustmentDrawerException ex){
            Log.e(TAG, ex.getMessage());
        }

        overlay.addOnPageChangeListener(pageChangeListener);

        DatabaseHandler dh = new DatabaseHandler(getActivity());
        Integer[] ids = dh.getImageIDs(ID);
        if(ids != null){
            galleryAdapter = new GalleryPagerAdapter(getActivity(), ids);
            overlay.setAdapter(galleryAdapter);
            if(mOverlayID > 0) overlay.setCurrentItem(galleryAdapter.getImagePosition(mOverlayID));
        }
        loadOverlay();
        mInitialized = true;

        return view;
    }

    private AdjustmentDrawer.AdjustmentDrawerListener adjustmentDrawerListener =
            new AdjustmentDrawer.AdjustmentDrawerListener() {
                @Override
                public void onOpacityChanged(int value) {
                    updateOpacity(value);
                }

                @Override
                public void onExposureChanged(int value) {

                }

                @Override
                public void onWhiteBalanceChanged(int value) {

                }
            };

    private void updateOpacity(int opacity){
        overlay.setAlpha(opacity / 100f);
        overlayOpacity.setProgress(opacity);
        overlayOpacityValue.setText(opacity + " %");
        mOverlayOpacity = opacity;
        drawer.updateItemValue(AdjustmentDrawer.DrawerItem.OVERLAY_OPACITY, opacity);
    }

    private void rotateControls(int orientation){

        mCurrDegree = 0;
        RotateAnimation anim;

        switch (orientation){
            case Surface.ROTATION_0:{
                mCurrDegree = 0;
                break;
            }
            case Surface.ROTATION_90:{
                mCurrDegree = 90;
                break;
            }
            case Surface.ROTATION_180:{
                mCurrDegree = 180;
                break;
            }
            case Surface.ROTATION_270:{
                mCurrDegree = 270;
                break;
            }
        }

        if(mCurrDegree != mLastDegree) {
            anim = new RotateAnimation(mLastDegree, mCurrDegree,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(mRotationDuration);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);

            overlayToggle.startAnimation(anim);
            flashToggle.startAnimation(anim);
            switchToggle.startAnimation(anim);
            //settings.startAnimation(anim);
            //overlaySettings.startAnimation(anim);
            adjustments.startAnimation(anim);
            close.startAnimation(anim);

            anim = new RotateAnimation(mLastDegree, mCurrDegree,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(mRotationDuration);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);

            capture.startAnimation(anim);
            drawer.rotateDrawer(mCurrDegree);

            mLastDegree = mCurrDegree;
        }
    }

    private void loadOverlay(){
        DatabaseHandler dh = new DatabaseHandler(getActivity());

        if(mOverlayID < 0){
            ProjectInfo inf = dh.getProject(ID);
            mOverlayID = inf.cover;
        }

        byte[] data = dh.getImageData(mOverlayID);
        Bitmap tmp = BitmapFactory.decodeByteArray(data, 0, data.length);

        if(tmp != null){
            overlay.setAlpha(mOverlayOpacity / 100f);
            mOverlayVisible = true;
            Log.d(TAG, "Overlay image set");
        } else {
            mOverlayVisible = false;

        }
        updateOverlay(mOverlayVisible);
    }

    public void saveSetting() {
        if (autoClearCache != null) {
            SharedPreferences.Editor editor = getActivity().getSharedPreferences(PREF_NAME_SETTINGS,
                    Context.MODE_PRIVATE).edit();

            editor.putBoolean(PREF_CLEAR_CACHE_ON_CLOSE, autoClearCache.isChecked());
            editor.putBoolean(PREF_SAVE_TO_GALLERY, saveToGallery.isChecked());
            editor.putBoolean(PREF_PROMPT_TO_SAVE, promptToSave.isChecked());
            editor.putBoolean(PREF_SAVE_ON_CLOSE, autoSaveSettings.isChecked());
            editor.putInt(PREF_CACHE_SIZE, cacheSize.getProgress());
            editor.commit();

            //These must go!
            getCameraControls().setCameraCatchSize(cacheSize.getProgress());
            getCameraControls().setSaveSettingsOnClose(autoSaveSettings.isChecked());

            editor = getActivity().getSharedPreferences(
                    String.valueOf(ID), Context.MODE_PRIVATE).edit();

            editor.putInt(PREF_OVERLAY_ID, mOverlayID);
            editor.putInt(PREF_OVERLAY_OPACITY, mOverlayOpacity);
            editor.putBoolean(PREF_OVERLAY_VISIBILITY, mOverlayVisible);
            editor.commit();
        }
    }

    public void loadSettings(){
        SharedPreferences pref = getActivity().getSharedPreferences(
                SettingsFragment.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);

        mPromptToSave = pref.getBoolean(SettingsFragment.PREF_PROMPT_TO_SAVE, true);
        mSaveToGallery = pref.getBoolean(SettingsFragment.PREF_SAVE_TO_GALLERY, false);
        mAutoClearCatche = pref.getBoolean(PREF_CLEAR_CACHE_ON_CLOSE, true);
        mAutoSaveSettings = pref.getBoolean(PREF_SAVE_ON_CLOSE, true);
        mCatchSize = pref.getInt(PREF_CACHE_SIZE, 1000);

        pref = getActivity().getSharedPreferences(String.valueOf(ID), Context.MODE_PRIVATE);

        mOverlayVisible = pref.getBoolean(PREF_OVERLAY_VISIBILITY, false);
        mOverlayOpacity = pref.getInt(PREF_OVERLAY_OPACITY, 75);
        mOverlayID = pref.getInt(PREF_OVERLAY_ID, -1);

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cs_open_camera:{
                    getPageControls().setCurrentItem(0, true);
                    break;
                }
                case R.id.cs_close_camera:{
                    getPageControls().close();
                    break;
                }
                case R.id.cs_clear_cache:{
                    getCameraControls().clearCameraCatch(true);
                }
                case R.id.camera_settings:{
                    getPageControls().setCurrentItem(1, true);
                    break;
                }
                case R.id.camera_overlay:{
                    if(mOverlayVisible) mOverlayVisible = false;
                    else mOverlayVisible = true;
                    updateOverlay(mOverlayVisible);
                    break;
                }
                case R.id.camera_close:{
                    getPageControls().close();
                    break;
                }
                case R.id.camera_adjustments:{
                    if(drawer.isDrawerOpen()) drawer.closeDrawer();
                    else drawer.openDrawer();
                    break;
                }
                case R.id.camera_picture:{
                    getCameraControls().takePicture();
                    break;
                }
                case R.id.camera_flash:{
                    updateFlash();
                    break;
                }
                case R.id.camera_switch:{
                    updateCameraType();
                    break;
                }
            }
        }
    };

    private void updateOverlayAdapter(){
        DatabaseHandler dh = new DatabaseHandler(getActivity());
        if(galleryAdapter == null){
            galleryAdapter = new GalleryPagerAdapter(getActivity(),dh.getImageIDs(ID));
            overlay.setAdapter(galleryAdapter);
        } else {
            galleryAdapter.updateImages(dh.getImageIDs(ID));
        }
    }

    private void updateOverlay(boolean visible){
        if(visible) {
            overlay.setVisibility(View.VISIBLE);
            overlayToggle.setImageResource(R.drawable.ic_filter_none_white_24dp);
        }
        else {
            overlay.setVisibility(View.GONE);
            overlayToggle.setImageResource(R.drawable.ic_filter_white_24dp);
        }
    }

    private void updateFlash(){
        switch (getCameraControls().getFlashType()){
            case FLASH_AUTO:{
                getCameraControls().setFlashType(SlyCamera.FlashType.FLASH_OFF);
                flashToggle.setImageResource(R.drawable.ic_flash_off_white_24dp);
                break;
            }
            case FLASH_OFF:{
                getCameraControls().setFlashType(SlyCamera.FlashType.FLASH_ON);
                flashToggle.setImageResource(R.drawable.ic_flash_on_white_24dp);
                break;
            }
            case FLASH_ON:{
                getCameraControls().setFlashType(SlyCamera.FlashType.FLASH_AUTO);
                flashToggle.setImageResource(R.drawable.ic_flash_auto_white_24dp);
                break;
            }
        }
    }

    private void updateCameraType(){
        switch(getCameraControls().getCameraType()){
            case CAMERA_BACK:{
                getCameraControls().setCameraType(SlyCamera.CameraType.CAMERA_FRONT);
                switchToggle.setImageResource(R.drawable.ic_camera_rear_white_24dp);
                break;
            }
            case CAMERA_FRONT:{
                getCameraControls().setCameraType(SlyCamera.CameraType.CAMERA_BACK);
                switchToggle.setImageResource(R.drawable.ic_camera_front_white_24dp);
                break;
            }
        }
    }

    private DatabaseListener.DataUpdateListener dataUpdateListener
            = new DatabaseListener.DataUpdateListener() {
        @Override
        public void onNewImageAdded(int projectID) {
            if(projectID == ID)updateOverlayAdapter();
        }
    };

    public SlyCameraListener getSlyCameraListener(){
        return slyCameraListener;
    }

    private SlyCameraListener slyCameraListener = new SlyCameraListener() {
        @Override
        public void onCameraRotated(int orientation) {
            if(mInitialized) rotateControls(orientation);
        }

        @Override
        public void onCameraCacheCleared(long sizeCleared, int nrOfFilesRemoved, boolean forcedClear) {
            if (forcedClear)
                Toast.makeText(getActivity(), "Cache cleared, " + sizeCleared / 1000 + " MB" +
                        " removed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onUnlockFocus() {

        }

        @Override
        public void onLockFocus() {

        }

        @Override
        public void onPreCapture() {

        }

        @Override
        public void onCaptureComplete(File imageFile) {
            if (promptToSave.isChecked()) {
                DisplayFragment displayFragment = DisplayFragment.newInstance(imageFile, ID,
                        saveToGallery.isChecked());
                displayFragment.show(getActivity().getFragmentManager(), "DisplayFragment");
            } else {
                Intent intent = new Intent(getActivity(), ImageSaveIntent.class);
                intent.putExtra(ImageSaveIntent.DELETE_TMP_IMAGE, true);
                intent.putExtra(ImageSaveIntent.IMAGE_ABSOLUTE_PATH, imageFile.getAbsolutePath());
                intent.putExtra(ImageSaveIntent.IMAGE_SAVE_TO_GALLERY, saveToGallery.isChecked());
                intent.putExtra(ImageSaveIntent.PROJECT_ID, ID);
                getActivity().startService(intent);
            }
        }

        @Override
        public void onError(int code, String msg) {

        }
    };

    private BroadcastReceiver cameraReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getType() == ImageSaveIntent.BROADCAST_TYPE) {
                updateOverlayAdapter();
            }
        }
    };

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            switch (seekBar.getId()){
                case R.id.cs_catch_size:{
                    cacheSizeValue.setText(progress + " MB");
                    mCatchSize = progress;
                    break;
                }
                case R.id.cs_overlay_opacity:{
                    updateOpacity(progress);
                }
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
