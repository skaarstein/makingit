package com.skaarstein.makinit.trycamera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

/**
 * Created by SlyOtis on 12.04.2016.
 */
public class GalleryAdapter extends ArrayAdapter<Integer> {

    // Declare Variables
    private Activity activity;
    private Integer[] ids;
    private DatabaseHandler dh;
    private int currPosition;
    private OnGalleryItemSelected listener;

    public interface OnGalleryItemSelected{
        void onGalleryItemSelected(int position, Bitmap image);
    }

    public GalleryAdapter(Activity context, int resource, Integer[] objects) {
        super(context, resource, objects);
        this.activity = context;
        updateImages(objects);
        this.dh = new DatabaseHandler(context);
    }

    public void setOnGalleryItemSelectdListener(OnGalleryItemSelected listener){
        this.listener = listener;
    }

    public void updateImages(Integer[] objects){
        if(objects != null) this.ids = objects;
    }

    public int getImageId(int position){
        if(position < 0) return ids[ids.length -1];
        return ids[position];
    }

    public int getImagePosition(int id){
        for(int i = 0; i < ids.length; i++){
            if(ids[i] == id) return i;
        }
        return ids.length -1 ;
    }

    public int getCurrPosition(){
        return currPosition;
    }

    @Override
    public int getCount() {
        if(ids == null) {
            return 0;
        }
        return ids.length;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) convertView = LayoutInflater.from(
                activity).inflate(R.layout.camera_overlay_item, parent, false);

        ImageView imageView = (ImageView)convertView;
        byte[] data = dh.getImageData(ids[position]);
        Bitmap tmp = ImageUtils.scaleImage(data, ImageResolution.P1080p);
        imageView.setImageBitmap(tmp);

        currPosition = position;
        if(listener != null) listener.onGalleryItemSelected(position, tmp);
        return convertView;
    }


}
