package com.skaarstein.makinit.trycamera;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by SlyOtis on 12.04.2016.
 */
public class VerticalPageTransformer implements ViewPager.PageTransformer {

    private View background;

    public VerticalPageTransformer(@NonNull View background) {
        this.background = background;
    }

    @Override
    public void transformPage(View view, float position) {
        if (position < -1) {
            view.setAlpha(0);
        } else if (position <= 1) {

            if (position < 0) {
                view.setAlpha(1 + position);
                background.setAlpha(1 + position - 0.45f);
            } else {
                view.setAlpha(1 - position);
                background.setAlpha(1 - position - 0.45f);
            }

            view.setTranslationX(view.getWidth() * -position);

            float yPosition = position * view.getHeight();
            view.setTranslationY(yPosition);
        } else {
            view.setAlpha(0);
        }
    }
}