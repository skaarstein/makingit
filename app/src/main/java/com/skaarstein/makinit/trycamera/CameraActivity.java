

package com.skaarstein.makinit.trycamera;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.slycamera.SettingsFragment;
import com.skaarstein.makinit.trycamera.CameraFragment.*;
import com.skaarstein.makinit.trycamera.CameraFragment;

public class CameraActivity extends Activity implements ViewPagerListener{

    private boolean isHome = false;
    private PagedCameraView.PagerControls controls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        int ID = getIntent().getExtras().getInt(Home.ID_KEY, -1);
        if(ID == -1) return;

        if (null == savedInstanceState) {
            CameraFragment cameraFragment = CameraFragment.newInstance(ID, this);
            controls = cameraFragment;
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            return;
        }

        if (isHome) {
            super.onBackPressed();
        } else {
            controls.setCurrentItem(0, true);
            return;
        }
    }

    @Override
    public void isHome(boolean value) {
        this.isHome = value;
    }

    @Override
    public void close() {
        super.onBackPressed();
    }

}
