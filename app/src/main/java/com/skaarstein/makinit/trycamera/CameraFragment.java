package com.skaarstein.makinit.trycamera;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.slycamera.SettingsFragment;
import com.skaarstein.makinit.slycamera.SlyCameraPagerAdapter;
import com.skaarstein.makinit.views.VerticalViewPager;
import com.skaarstein.slycamera.SlyCamera;

/**
 * Created by Martin on 03.04.2016.
 */
public class CameraFragment extends Fragment implements PagedCameraView.PagerControls{

    private static final String TAG = CameraFragment.class.getName();
    private SlyCamera slyCamera;
    private PagedCameraView cameraView;
    private VerticalViewPager pager;
    private ViewPagerListener pagerListener;
    private int ID;

    public static CameraFragment newInstance(int ID, ViewPagerListener listener){
        CameraFragment cameraFragment = new CameraFragment();
        cameraFragment.ID = ID;
        cameraFragment.pagerListener = listener;
        return cameraFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_try_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(savedInstanceState == null) {

            slyCamera = (SlyCamera) view.findViewById(R.id.slyCamera);
            slyCamera.startCamera(getActivity());

            pager = (VerticalViewPager) view.findViewById(R.id.slyCamera_pager);
            cameraView = new PagedCameraView(getActivity(), ID, this, slyCamera);
            pager.setAdapter(cameraView);
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if(position == 0)pagerListener.isHome(true);
                    else pagerListener.isHome(false);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            slyCamera.addSlyCameraListener(cameraView.getSlyCameraListener());
            pager.setPageTransformer(true,
                    new VerticalPageTransformer(view.findViewById(R.id.slyCamera_pager_bck)));
        }
    }

    public interface ViewPagerListener{
        void isHome(boolean value);
        void close();
    }

    @Override
    public void setCurrentItem(int index, boolean smooth) {
        pager.setCurrentItem(index, smooth);
        if(index == 0)pagerListener.isHome(true);
        else pagerListener.isHome(false);
    }

    @Override
    public void close() {
        pagerListener.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(cameraView != null)
            cameraView.loadSettings();
        if(slyCamera != null)
            slyCamera.startCamera(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();

        if(cameraView != null)
            cameraView.saveSetting();

        if(slyCamera != null)
            slyCamera.stopCamera();
    }
}
