package com.skaarstein.makinit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.skaarstein.makinit.camera.CameraActivity;
import com.skaarstein.makinit.edit.ProjectFragment;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.share.ShareFragment;
import com.skaarstein.makinit.slycamera.SlyCameraActivity;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

/**
 * Created by SlyOtis on 01.02.2016.
 */
public class ProjectViewAdapter extends CursorAdapter implements ProjectFragment.OnDataSetChangeListener{

    private static final String TAG = ProjectViewAdapter.class.getName();
    private final Activity activity;

    public ProjectViewAdapter(Activity activity, Cursor c) {
        super(activity, c, true);
        this.activity = activity;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        SwipeLayout swipeLayout =  (SwipeLayout)
                LayoutInflater.from(context).inflate(R.layout.project_card, null);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        return swipeLayout;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        SwipeLayout v = (SwipeLayout)view;
        ProjectInfo prj = new ProjectInfo(cursor.getInt(0), cursor.getString(4));
        prj.name = cursor.getString(1);
        prj.description = cursor.getString(2);

        ((TextView)v.findViewById(R.id.card_title)).setText(prj.name);
        ((TextView)v.findViewById(R.id.card_created)).setText(prj.created);
        ((TextView)v.findViewById(R.id.card_description)).setText(prj.description);
        ImageView cover = (ImageView)view.findViewById(R.id.card_cover);

        CoverLoader coverLoader = new CoverLoader(context, cover);
        coverLoader.execute(cursor.getInt(5));

        final int ID = cursor.getInt(0);
        view.findViewById(R.id.card_delete).setOnClickListener(
                new ProjectDeleter(ID));

        v.findViewById(R.id.card_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectFragment.newInstance(ID, ProjectViewAdapter.this)
                .show(activity.getFragmentManager(), "Edit");
            }
        });

        v.findViewById(R.id.card_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new DatabaseHandler(activity).getImageCount(ID) < 2){
                    Toast.makeText(activity,
                            R.string.share_error_missing_images, Toast.LENGTH_LONG).show();
                } else {
                    activity.getFragmentManager().beginTransaction()
                            .replace(R.id.home_container, ShareFragment.newInstance(ID))
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left,
                                    R.anim.slide_in_right, R.anim.slide_out_right)
                            .addToBackStack("Share")
                            .commit();
                }
            }
        });

        v.findViewById(R.id.card_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(activity, R.string.unavaliable, Toast.LENGTH_SHORT).show();
                /**
                activity.getFragmentManager().beginTransaction()
                        .replace(R.id.home_container, GalleryFragment.newInstance(ID))
                        .addToBackStack("Gallery")
                        .commit();
                 **/
            }
        });

        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, com.skaarstein.makinit.trycamera.CameraActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Home.ID_KEY, ID);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public void onDataChanged() {
        DatabaseHandler dh = new DatabaseHandler(activity);
        swapCursor(dh.getProjectCursor());
        dh.close();
    }

    public class ProjectDeleter extends AsyncTask<Integer, Integer, Boolean> implements View.OnClickListener{

        private final int ID;

        public ProjectDeleter(int ID) {
            this.ID = ID;
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            DatabaseHandler dh = new DatabaseHandler(activity);
            return dh.deleteProject(params[0]) > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            DatabaseHandler dh = new DatabaseHandler(activity);
            swapCursor(dh.getProjectCursor());
            dh.close();
        }

        @Override
        public void onClick(View v) {
            this.execute(ID);
        }
    }

    public class CoverLoader extends AsyncTask<Integer, Integer, Bitmap> {

        private final Context context;
        private final ImageView imageView;

        public CoverLoader(Context context, ImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            Log.d(TAG, "Retrieving cover");
            DatabaseHandler dh = new DatabaseHandler(context);
            byte[] data = dh.getImageData(params[0]);
            if(data != null)
                return ImageUtils.scaleImage(data, ImageResolution.P480p);
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
                Log.d(TAG, "Updating cover");
            }
        }
    }
}
