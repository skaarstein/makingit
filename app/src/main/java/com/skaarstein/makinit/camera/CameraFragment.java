
package com.skaarstein.makinit.camera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class CameraFragment extends Fragment implements View.OnClickListener, FragmentCompat.OnRequestPermissionsResultCallback, DisplayFragment.OnUpdateOverlayListener{

    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final SparseArray<String> CAMERAS = new SparseArray<>();
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private enum FlashType{
        FLASH_AUTO,
        FLASH_OFF,
        FLASH_ON
    }
    /**
     * Tag for the {@link Log}.
     */
    private static final String TAG = "CameraCapture";

    /**
     * CameraActivity state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * CameraActivity state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * CameraActivity state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * CameraActivity state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * CameraActivity state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;

    /**
     * Max preview width that is guaranteed by CameraFragment API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by CameraFragment API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private static final int CAMERA_FRONT = 0;
    private static final int CAMERA_REAR = 1;

    private static FlashType flashType;
    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            openCamera(width, height, CAMERAS.get(mCameraType));
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
           configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };

    /**
     * ID of the current {@link CameraDevice}.
     */
    private String mCameraId;

    /**
     * An {@link CameraView} for camera preview.
     */
    private CameraView cameraView;

    /**
     * A {@link CameraCaptureSession } for camera preview.
     */
    private CameraCaptureSession mCaptureSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * The {@link Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

    };

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader mImageReader;

    private int ID;


    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(final ImageReader reader) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingPanel.setVisibility(View.VISIBLE);
                    loadingPanel.setClickable(true);
                }
            });

            mBackgroundHandler.post(new Runnable() {
                @Override
                public void run() {

                    final Activity activity = getActivity();
                    Image lastCapture = reader.acquireLatestImage();
                    ByteBuffer buffer = lastCapture.getPlanes()[0].getBuffer();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);

                    FileOutputStream output = null;
                    File outputFile = null;
                    try {
                        File cameraDir = new File(
                                Environment.getExternalStoragePublicDirectory(
                                        Environment.DIRECTORY_DCIM).toString());

                        outputFile = new File(cameraDir, String.valueOf(ID) + ".jpg");
                        if(outputFile.exists()){
                            Log.d(TAG, "Deleting old file...");
                            outputFile.delete();
                        }

                        output = new FileOutputStream(outputFile);
                        output.write(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        lastCapture.close();
                        final File displayFile = outputFile;
                        try {
                            output.close();
                        } catch (IOException e) {
                            Log.e(TAG, e.getMessage());
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DisplayFragment display =
                                        DisplayFragment.newInstance(displayFile,
                                                ID, CameraFragment.this, mCameraType);
                                display.show(getFragmentManager(), "Display");
                            }
                        });
                    }
                }
            });

            mBackgroundHandler.post(new Runnable() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingPanel.setVisibility(View.GONE);
                        }
                    });
                }
            });
        }

    };

    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #mPreviewRequestBuilder}
     */
    private CaptureRequest mPreviewRequest;

    public ImageView screenOverlay;

    private Bitmap screen_overlay_image;

    private RelativeLayout loadingPanel;

    private FrameLayout overlayOpacity;

    private TextView overlayOpacityText;

    private ImageButton cameraSwitch;
    /**
     * The current state of camera state for taking pictures.
     *
     * @see #mCaptureCallback
     */
    private int mState = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * Whether the current camera device supports Flash or not.
     */
    private boolean mFlashSupported;

    private boolean mFrontCameraSupported;

    private boolean mCamerasLoaded;

    private int mCameraType;

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            Size size = Collections.min(bigEnough, new CompareSizesByArea());
            return size;
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    public static CameraFragment newInstance() {
        return new CameraFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ID = getActivity().getIntent().getExtras().getInt(Home.ID_KEY, -1);
        if(ID == -1) return;

        view.findViewById(R.id.camera_picture).setOnClickListener(this);
        view.findViewById(R.id.camera_opacity).setOnClickListener(this);
        view.findViewById(R.id.camera_flash).setOnClickListener(this);
        view.findViewById(R.id.camera_close).setOnClickListener(this);
        cameraSwitch = (ImageButton)view.findViewById(R.id.camera_switch);
        screenOverlay = (ImageView)view.findViewById(R.id.screen_overlay);
        cameraView = (CameraView) view.findViewById(R.id.texture);
        loadingPanel = (RelativeLayout)view.findViewById(R.id.loadingPanel);
        overlayOpacity = (FrameLayout)view.findViewById(R.id.screen_overlay_opacity_container);
        overlayOpacityText = (TextView)view.findViewById(R.id.screen_overlay_opacity_text);
        cameraView.setOnClickListener(this);
        cameraSwitch.setOnClickListener(this);

        flashType = FlashType.FLASH_AUTO;

        if(screen_overlay_image == null){
            OverlayLoader overlayLoader = new OverlayLoader(getActivity(), true);
            overlayLoader.execute(ID);
        }else {
            screenOverlay.setImageBitmap(screen_overlay_image);
        }

        screenOverlay.setOnTouchListener(new View.OnTouchListener() {
            float location;
            float last_y = 0;
            float scroll_area;
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) overlayOpacityText.getLayoutParams();

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        overlayOpacity.setVisibility(View.VISIBLE);
                        scroll_area = v.getHeight() * 0.4f;
                        location = (int) event.getY() - (int) (scroll_area * (1 - screenOverlay.getAlpha()));
                        return true;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        last_y = 1 - (((int) event.getY() - location) / scroll_area);
                        if (last_y < 0) last_y = 0;
                        if (last_y > 1) last_y = 1;
                        screenOverlay.setAlpha(last_y);
                        overlayOpacityText.setText(Math.ceil(last_y * 100) + "%");
                        params.topMargin = (int) event.getRawY() - overlayOpacityText.getHeight() - 200;
                        params.leftMargin = (int) event.getRawX() - (overlayOpacityText.getWidth() / 2);
                        overlayOpacityText.setLayoutParams(params);
                        return true;
                    }
                    case MotionEvent.ACTION_UP: {
                        overlayOpacity.setVisibility(View.GONE);
                        return true;
                    }

                }

                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThread();

        Log.d(TAG, "Resumed");

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if(!mCamerasLoaded){
            findAvaliableCameras();
            mCamerasLoaded = true;
        }
        if (cameraView.isAvailable()) {
            openCamera(cameraView.getWidth(), cameraView.getHeight(), CAMERAS.get(mCameraType));
        } else {
            cameraView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private boolean requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                return false;
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_PERMISSION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return true;
            }
        }else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance(getString(R.string.request_permission))
                        .show(getFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private static Size findRelativeSize(Size[] choices, Size toMatch) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = toMatch.getWidth();
        int h = toMatch.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= w && option.getHeight() <= h){
                bigEnough.add(option);
            } else {
                notBigEnough.add(option);
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            Size size = Collections.max(bigEnough, new CompareSizesByArea());
            return size;
        } else if (notBigEnough.size() > 0) {
            return Collections.min(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    private void findAvaliableCameras(){
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null) {
                    switch (facing) {
                        case CameraCharacteristics.LENS_FACING_FRONT: {
                            CAMERAS.append(CAMERA_FRONT, cameraId);
                            mFrontCameraSupported = true;
                            break;
                        }
                        case CameraCharacteristics.LENS_FACING_BACK: {
                            CAMERAS.append(CAMERA_REAR, cameraId);
                            mCameraType = CAMERA_REAR;
                            break;
                        }
                    }
                }
            }
            if(mFrontCameraSupported) cameraSwitch.setVisibility(View.VISIBLE);
        }catch (CameraAccessException e) {
            Log.e(TAG, e.getMessage());
        }
    }
    /**
     * Sets up member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    private void setUpCameraOutputs(int width, int height, String cameraId) {

        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics
                    = manager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            if (map == null) {
                return;
            }

            Size resolution = findRelativeSize(map.getOutputSizes(ImageFormat.JPEG),
                    new Size(1920, 1080));

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            int displayRotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            int sensorOrientation =
                    characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            boolean swappedDimensions = false;

            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (sensorOrientation == 90 || sensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (sensorOrientation == 0 || sensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e(TAG, "Display rotation is invalid: " + displayRotation);
            }

            Point displaySize = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);

            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
                Log.d(TAG, "Swapped: " + width + "x" + height + ", "
                        + maxPreviewWidth + "x" + maxPreviewHeight);
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
            // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
            // garbage capture data.
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, Collections.max(
                            Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                            new CompareSizesByArea()));

            mImageReader = ImageReader.newInstance(
                    resolution.getWidth(),
                    resolution.getHeight(),
                    ImageFormat.JPEG, /*maxImages*/2);

            Log.d(TAG, "Image resolution: " + resolution.getWidth() +"x" + resolution.getHeight());
            Log.d(TAG, "Preview size: " + mPreviewSize.getHeight() + "x" + mPreviewSize.getWidth());

            mImageReader.setOnImageAvailableListener(
                    mOnImageAvailableListener, mBackgroundHandler);

            // We fit the aspect ratio of TextureView to the size of preview we picked.
            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                cameraView.setAspectRatio(
                        mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                cameraView.setAspectRatio(
                        mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }
            // Check if the flash is supported.
            Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
            mFlashSupported = available == null ? false : available;

            mCameraId = cameraId;
            return;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getFragmentManager(), FRAGMENT_DIALOG);
        }
    }

    /**
     * Opens the camera specified by {@link CameraCapture#mCameraId}.
     */
    private void openCamera(int width, int height, String cameraID) {
        if (!requestCameraPermission()) {
            return;
        }

        setUpCameraOutputs(width, height, cameraID);
        configureTransform(width, height);


        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            final SurfaceTexture texture = cameraView.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                //setFlashMode(mPreviewRequestBuilder);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOverlayUpdated(Bitmap overlay) {
        Log.d(TAG, "Updating overlay");
        screen_overlay_image = overlay;
        screenOverlay.setImageBitmap(screen_overlay_image);
    }

    private class GradientLoader extends AsyncTask<Bitmap, Integer, Bitmap>{
        @Override
        protected Bitmap doInBackground(Bitmap... params) {

            Bitmap tmp = params[0];
            int w = tmp.getWidth();
            int h = tmp.getHeight();
            Canvas canvas = new Canvas(tmp);

            canvas.drawBitmap(tmp, 0, 0, null);

            Paint framePaint = new Paint();
            for(int i = 1; i < 5; i++){
                setFramePaint(framePaint, i, w, h);
                canvas.drawPaint(framePaint);
            }

            return tmp;
        }

        private void setFramePaint(Paint p, int side, float iw, float ih){
            // paint, side of rect, image width, image height

            p.setShader(null);
            p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

            float borderSize = 0.1f; //relative size of border
            //use the smaller image size to calculate the actual border size
            float bSize = (iw > ih)? ih * borderSize: ih * borderSize;
            float g1x = 0;
            float g1y = 0;
            float g2x = 0;
            float g2y = 0;
            int c1 = 0, c2 = 0;

            if (side == 1){
                //left
                g1x = 0;
                g1y = ih/2;
                g2x = bSize;
                g2y = ih/2;
                c1 = Color.TRANSPARENT;
                c2 = Color.BLACK;

            }else if(side == 2){
                //top
                g1x = iw/2;
                g1y = 0;
                g2x = iw/2;
                g2y = bSize;
                c1 = Color.TRANSPARENT;
                c2 = Color.BLACK;


            }else if(side == 3){
                //right
                g1x = iw;
                g1y = ih/2;
                g2x = iw - bSize;
                g2y = ih/2;
                c1 = Color.TRANSPARENT;
                c2 = Color.BLACK;


            }else if(side == 4){
                //bottom
                g1x = iw/2;
                g1y = ih;
                g2x = iw/2;
                g2y = ih - bSize;
                c1 = Color.TRANSPARENT;
                c2 = Color.BLACK;
            }

            p.setShader(new LinearGradient(g1x, g1y, g2x, g2y, c1, c2, Shader.TileMode.CLAMP));

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            screenOverlay.setImageBitmap(screen_overlay_image);
        }
    }
    private class OverlayLoader extends AsyncTask<Integer, Integer, Bitmap> {

        private final Context context;
        private boolean centerFit;
        private boolean gradient;

        public OverlayLoader(Context context, boolean gradient) {
            this.context = context;
            this.gradient = gradient;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            try{
                DatabaseHandler dh = new DatabaseHandler(context);
                ProjectInfo inf = dh.getProject(params[0]);
                byte[] data = dh.getImageData(inf.cover);
                Bitmap tmp = BitmapFactory.decodeByteArray(data, 0, data.length);

                boolean landscape;
                if(tmp.getWidth() > tmp.getHeight()) landscape = true;
                else landscape = false;

                switch (getResources().getConfiguration().orientation){
                    case Configuration.ORIENTATION_PORTRAIT:
                        if(landscape)centerFit = true;
                        else centerFit = false;
                        break;
                    case Configuration.ORIENTATION_LANDSCAPE:
                        if(landscape) centerFit = false;
                        else centerFit = true;
                        break;
                }

                return tmp;
            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap image) {
            super.onPostExecute(image);
            if(image != null){
                screen_overlay_image = image;
                screenOverlay.setImageBitmap(image);
                if (centerFit) screenOverlay.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        }
    }
    /**
     * Configures the necessary {@link Matrix} transformation to `cameraView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `cameraView` is fixed.
     *
     * @param viewWidth  The width of `cameraView`
     * @param viewHeight The height of `cameraView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        
        if (null == cameraView || null == mPreviewSize) {
            return;
        }

        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        float scale = Math.max((float) viewHeight / mPreviewSize.getHeight(),
                (float) viewWidth / mPreviewSize.getWidth());

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
            Log.d(TAG, "Scale: " + scale);
        } else {
            scale = Math.max((float) viewHeight / mPreviewSize.getWidth(),
                    (float) viewWidth / mPreviewSize.getHeight());
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            matrix.postScale(scale, scale, centerX, centerY);

            Log.d(TAG, "Scale: " + scale);

            if (Surface.ROTATION_180 == rotation) {
                matrix.postRotate(180, centerX, centerY);
                Log.d(TAG, "Scale ro 180: " + scale);
            }
        }
        cameraView.setTransform(matrix);
        Log.d(TAG, "configurationTransform " + viewWidth + ", " + viewHeight);
    }

    /**
     * Initiate a still image capture.
     */
    private void takePicture() {
        lockFocus();
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            captureStillPicture();
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */

    private void captureStillPicture() {
        try{
            if (null == mCameraDevice) {
                return;
            }

            mState = STATE_PICTURE_TAKEN;
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            setFlashMode(captureBuilder);

            // Orientation
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    unlockFocus();
                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), CaptureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */

    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            setFlashMode(mPreviewRequestBuilder);
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                    mBackgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    private boolean overlayVissible = true;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.camera_picture: {
                takePicture();
                break;
            }
            case R.id.camera_opacity:{
                if(overlayVissible){
                    overlayVissible = false;
                    overlayOpacity.setVisibility(View.GONE);
                    screenOverlay.setVisibility(View.GONE);
                } else {
                    overlayVissible = true;
                    overlayOpacity.setVisibility(View.VISIBLE);
                    screenOverlay.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.camera_close:{
                getActivity().finish();
                break;
            }
            case R.id.camera_flash:{
                switch (flashType){
                    case FLASH_AUTO:{
                        flashType = FlashType.FLASH_OFF;
                        ((ImageButton)view).setImageResource(R.drawable.ic_flash_off_white_24dp);
                        break;
                    }
                    case FLASH_OFF:{
                        flashType = FlashType.FLASH_ON;
                        ((ImageButton)view).setImageResource(R.drawable.ic_flash_on_white_24dp);
                        break;
                    }
                    case FLASH_ON:{
                        flashType = FlashType.FLASH_AUTO;
                        ((ImageButton)view).setImageResource(R.drawable.ic_flash_auto_white_24dp);
                        break;
                    }
                }
                break;
            }
            case R.id.camera_switch:{
                switch (mCameraType){
                    case CAMERA_FRONT:{
                        closeCamera();
                        openCamera(cameraView.getWidth(), cameraView.getHeight(), CAMERAS.get(CAMERA_REAR));
                        cameraSwitch.setImageResource(R.drawable.ic_camera_front_white_24dp);
                        mCameraType = CAMERA_REAR;
                        break;
                    }
                    case CAMERA_REAR:{
                        closeCamera();
                        openCamera(cameraView.getWidth(), cameraView.getHeight(), CAMERAS.get(CAMERA_FRONT));
                        cameraSwitch.setImageResource(R.drawable.ic_camera_rear_white_24dp);
                        mCameraType = CAMERA_FRONT;
                        break;
                    }
                }
                break;
            }
            case R.id.texture:{
                manualFocus();
                break;
            }
        }
    }
    private void manualFocus(){

    }
    private void setFlashMode(CaptureRequest.Builder requestBuilder) {
        if (mFlashSupported) {
            switch (flashType){
                case FLASH_AUTO:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                    break;
                }
                case FLASH_OFF:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON);
                    break;
                }
                case FLASH_ON:{
                    requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                            CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                    break;
                }
            }

        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    /**
     * Shows an error message dialog.
     */
    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity())
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.finish();
                            }
                        }
                    })
                    .create();
        }

    }

    /**
     * Shows OK/Cancel confirmation dialog about camera permission.
     */
    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

}