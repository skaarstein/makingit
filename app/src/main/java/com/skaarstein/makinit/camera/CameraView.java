
package com.skaarstein.makinit.camera;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;

/**
 * A {@link TextureView} that can be adjusted to a specified aspect ratio.
 */
public class CameraView extends TextureView {

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    private static final String TAG = CameraView.class.getName();

    public CameraView(Context context) {
        this(context, null);
    }

    public CameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        Log.d(TAG, width + "x" + height + " is aspect ratio");
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        Log.d(TAG, width + "," + height + " is measured dimension");
        Log.d(TAG, "Aspect ratio: " + (float)mRatioHeight / mRatioWidth + ", " + (float)mRatioWidth / mRatioHeight);
        Log.d(TAG, "Aspect ratio relative: " + (float)width / mRatioWidth + ", " + (float)height / mRatioHeight);
        Log.d(TAG, "Aspect ratio relative revert: " + (float)mRatioWidth / width + ", " + (float) mRatioHeight/ height);


        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height) {
                setMeasuredDimension(width * mRatioHeight / mRatioWidth, height);
                Log.d(TAG, "Measured dimension min is: " + width * mRatioHeight / mRatioWidth + ", " + height);
            } else {
                setMeasuredDimension(width, height * mRatioWidth / mRatioHeight);
                Log.d(TAG, "Measured dimension max is: " + width + ", " + height * mRatioWidth / mRatioHeight);
            }
        }
    }

}
