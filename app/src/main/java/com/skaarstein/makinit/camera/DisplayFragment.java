package com.skaarstein.makinit.camera;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.edit.ImageFragment;
import com.skaarstein.makinit.edit.ImageInfo;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ExifUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DisplayFragment extends DialogFragment implements View.OnClickListener,
        ImageFragment.OnInfoUpdatedListener, View.OnTouchListener{

    private static final String TAG = DisplayFragment.class.getName();
    private ImageView imageDisplay;
    private int prjID;
    private OnUpdateOverlayListener listener;
    private File tempFile;
    private Bitmap tempImage;
    private String tmpTitle;
    private String tmpDesc;
    private boolean isFrontCamera;
    private static int FLING_TREESHOLD = 200;

    public interface OnUpdateOverlayListener{
        void onOverlayUpdated(Bitmap overlay);
    }

    public static DisplayFragment newInstance(File tempFile, int prjID,
                                                  OnUpdateOverlayListener listener, int cameraType){

        DisplayFragment display = new DisplayFragment();
        display.tempFile = tempFile;
        display.prjID = prjID;
        display.listener = listener;
        display.isFrontCamera = cameraType == 0;
        return display;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.Dialog_Display);

        int orientation = getResources().getConfiguration().orientation;
        switch (orientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageDisplay = (ImageView)view.findViewById(R.id.display_image);
        view.findViewById(R.id.display_accept).setOnClickListener(this);
        view.findViewById(R.id.display_discard).setOnClickListener(this);
        view.findViewById(R.id.display_edit_info).setOnClickListener(this);

        ImageLoader imageLoader = new ImageLoader();
        imageLoader.execute(tempFile);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_display_new, container, false);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.display_accept:
                ImageSaver imageUpdater = new ImageSaver();
                imageUpdater.execute();
                break;
            case R.id.display_discard:
                if(tempFile != null) tempFile.delete();
                dismiss();
                break;
            case R.id.display_edit_info:{
                ImageFragment imageFragment = ImageFragment.newInstance(tempImage,
                        tmpTitle, tmpDesc, this);
                imageFragment.show(getFragmentManager(), "ImageInfoEdit");
                break;
            }
        }
    }

    float down_x = 0;
    float last_x = 0;
    float new_x = 0;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Matrix old = null;
        float angle, new_tresh = 0;

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:{
                down_x = event.getX();
                Log.d(TAG, "Down: " + down_x);
                return  true;
            }
            case MotionEvent.ACTION_MOVE:{
                new_x = event.getX();

                if(new_x > down_x){
                    //fling right
                    Matrix m = old = imageDisplay.getImageMatrix();
                    imageDisplay.setScaleType(ImageView.ScaleType.MATRIX);
                    new_tresh = new_x - down_x;
                    angle = (new_tresh/imageDisplay.getWidth())*45;
                    m.postRotate(angle, imageDisplay.getWidth(), imageDisplay.getHeight());
                    imageDisplay.setAlpha((100 - angle)/100f);
                    imageDisplay.setImageMatrix(m);
                    imageDisplay.invalidate();
                    Log.d(TAG, "Rotating image max: " + angle);
                } else if(new_x < down_x) {
                    //fling left
                    Matrix m = old = imageDisplay.getImageMatrix();
                    new_tresh = down_x - new_x;
                    angle = (new_tresh/imageDisplay.getWidth())*45;
                    m.postRotate(-angle, 0, imageDisplay.getHeight());
                    imageDisplay.setAlpha((100 - angle)/100f);
                    imageDisplay.setImageMatrix(m);
                    imageDisplay.invalidate();
                    Log.d(TAG, "Rotating image min: " + angle);
                }
                return true;
            }
            case MotionEvent.ACTION_UP:{
                if(new_tresh > FLING_TREESHOLD){
                    imageDisplay.setImageMatrix(old);
                    imageDisplay.invalidate();
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void onInfoUpdated(String title, String description) {
        tmpDesc = description;
        tmpTitle = title;
    }

    @Override
    public void onInfoEditClose() {
        ImageSaver imageUpdater = new ImageSaver();
        imageUpdater.execute();
    }

    private class ImageLoader extends AsyncTask<File, Integer, Bitmap>{

        @Override
        protected Bitmap doInBackground(File... params) {
            File inputFile = params[0];
            Bitmap tempBitmap = BitmapFactory.decodeFile(inputFile.getAbsolutePath());
            Bitmap finalBitmap = ExifUtil.rotateBitmap(inputFile.getAbsolutePath(),
                    tempBitmap);
            inputFile.delete();
            return finalBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            tempFile = null;
            tempImage = bitmap;

            Matrix m = imageDisplay.getImageMatrix();
            imageDisplay.setScaleType(ImageView.ScaleType.MATRIX);
            RectF displayRect = new RectF(0, 0, imageDisplay.getWidth(), imageDisplay.getHeight());
            RectF imageRect = new RectF(0, 0, tempImage.getWidth(), tempImage.getHeight());
            m.setRectToRect(imageRect, displayRect, Matrix.ScaleToFit.FILL);
            imageDisplay.setImageBitmap(tempImage);
            imageDisplay.setImageMatrix(m);
        }
    }

    public class ImageSaver extends AsyncTask<Integer, Integer, Bitmap>{


        private int generateKey() {
            int uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
            return uniqueId;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {

            Activity activity = getActivity();
            DatabaseHandler dh = new DatabaseHandler(activity);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            int imageID = generateKey();
            ImageInfo img = new ImageInfo(imageID, prjID, sdf.format(new Date()));

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            try {
                tempImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] data = stream.toByteArray();

                img.title = tmpTitle;
                img.description = tmpDesc;
                img.imageData = data;

                Log.d(TAG, data.length + ", " + data.toString());

                dh.addImage(img);

                ProjectInfo prj = dh.getProject(prjID);
                prj.cover = img.ID;
                dh.updateProject(prj);

                return tempImage;
            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }finally {
                try{
                    stream.close();
                }catch (IOException ex){
                    Log.e(TAG, ex.getMessage());
                }
            }
        }

        @Override
        protected void onPostExecute(Bitmap overlay) {
            super.onPostExecute(overlay);
            listener.onOverlayUpdated(overlay);
            if(tempFile != null) tempFile.delete();
            dismiss();
        }
    }
}
