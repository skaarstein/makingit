

package com.skaarstein.makinit.camera;

import android.app.Activity;
import android.os.Bundle;

import com.skaarstein.makinit.R;

public class CameraActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, CameraFragment.newInstance())
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0) {
           getFragmentManager().popBackStack();
           return;
        }

        super.onBackPressed();
    }
}
