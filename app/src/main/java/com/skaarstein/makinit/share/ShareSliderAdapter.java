package com.skaarstein.makinit.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skaarstein.makinit.gallery.GalleryImage;
import com.skaarstein.makinit.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Martin Ottersland on 3/3/2016.
 */
public class ShareSliderAdapter extends RecyclerView.Adapter<ShareSliderAdapter.ViewHolder>{

    private final Context context;
    private ArrayList<GalleryImage> images;
    private ArrayList<SliderAdapterListener> listeners = new ArrayList<>();
    private int slideSize;

    public ShareSliderAdapter(Context context, GalleryImage[] images, SliderAdapterListener listener) {
        this.context = context;
        this.images = new ArrayList<>(Arrays.asList(images));
        this.listeners.add(listener);
    }

    public interface SliderAdapterListener{
        void onSlideSelected(int position, boolean clicked);
        void onSlideDelete(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SliderAdapterListener{
        public ImageView image;
        private ImageView imageBorder;
        private ImageView delete;
        private boolean selected = false;
        public int position;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            image = (ImageView) view.findViewById(R.id.share_slide_image);
            imageBorder = (ImageView) view.findViewById(R.id.share_slide_image_border);
            delete = (ImageView)view.findViewById(R.id.share_slide_delete);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(selected)
                listener.onSlideDelete(position);
            else
                listener.onSlideSelected(position, true);
        }

        @Override
        public void onSlideDelete(int position) {

        }

        @Override
        public void onSlideSelected(int position, boolean clicked) {
            if(position != this.position){
                imageBorder.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
                if(selected) {
                    delete.animate().alpha(0).setDuration(350).start();
                    selected = false;
                }
            } else {
                imageBorder.setVisibility(View.VISIBLE);
                if (clicked) {
                    delete.setVisibility(View.VISIBLE);
                    delete.animate().alpha(1).setDuration(350).start();
                    selected = true;
                }
            }
        }
    }

    private SliderAdapterListener listener = new SliderAdapterListener() {
        @Override
        public void onSlideSelected(int position, boolean clicked) {
            for(SliderAdapterListener s: listeners){
                s.onSlideSelected(position, clicked);
            }
        }

        @Override
        public void onSlideDelete(int position) {
            for(SliderAdapterListener s: listeners){
                s.onSlideDelete(position);
            }
        }
    };

    public void deleteSlide(int position){
        images.remove(position);
        notifyDataSetChanged();
        listener.onSlideSelected(0, true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.share_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        listeners.add(viewHolder);
        view.setLayoutParams(new ViewGroup.LayoutParams(
                (int)context.getResources().getDimension(R.dimen.share_slide_size),
                ViewGroup.LayoutParams.MATCH_PARENT));
        slideSize = view.getWidth();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.image.setImageBitmap(images.get(position).image);
        holder.imageBorder.setVisibility(View.GONE);
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public Bitmap[] getImages(){
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        for(int i = 0; i < getItemCount(); i++){
            bitmaps.add(images.get(i).image);
        }
        return bitmaps.toArray(new Bitmap[bitmaps.size()]);
    }

    public GalleryImage getImage(int position){
        if(position > images.size() - 1) return images.get(0);
        return images.get(position);
    }
    public int getSlideSize(){
        return slideSize;
    }

    public void selectItem(int position){
        listener.onSlideSelected(position, false);
    }
}
