package com.skaarstein.makinit.share;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.skaarstein.makinit.R;

/**
 * Created by pc on 14.03.2016.
 */
public class GifSettingsFragment extends DialogFragment implements SeekBar.OnSeekBarChangeListener{

    private int duration;
    private int quality;
    private int repeat;
    private TextView durationValue;
    private TextView qualityValue;
    private TextView repeatValue;
    private OnGifSettingsUpdated listener;

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.gif_edit_duration:{
                if(progress > 0){
                    duration = progress;
                    durationValue.setText(String.valueOf(duration));
                    listener.onGifSettingsUpdated(duration, quality, repeat);
                }
                break;
            }
            case R.id.gif_edit_quality:{
                if(progress > 0){
                    quality = progress;
                    qualityValue.setText(String.valueOf(quality));
                    listener.onGifSettingsUpdated(duration, quality, repeat);
                }
                break;
            }
            case R.id.gif_edit_repeat:{
                repeat = progress;
                if(repeat == 0){
                    repeatValue.setText("inf");
                } else repeatValue.setText(String.valueOf(repeat));
                listener.onGifSettingsUpdated(duration, quality, repeat);
                break;
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface OnGifSettingsUpdated{
        void onGifSettingsUpdated(int duration, int quality, int repeat);
    }

    public static GifSettingsFragment newInstance(
            @NonNull OnGifSettingsUpdated listener, int duration, int quality, int repeat){
        GifSettingsFragment settingsFragment = new GifSettingsFragment();
        settingsFragment.listener = listener;
        settingsFragment.duration = duration;
        settingsFragment.quality = quality;
        settingsFragment.repeat = repeat;
        return settingsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.Dialog_Gif);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window =getDialog().getWindow();
        window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gif_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SeekBar sDuration = (SeekBar) view.findViewById(R.id.gif_edit_duration);
        sDuration.setOnSeekBarChangeListener(this);
        SeekBar sQuality = (SeekBar)view.findViewById(R.id.gif_edit_quality);
        sQuality.setOnSeekBarChangeListener(this);
        SeekBar sRepeat = (SeekBar)view.findViewById(R.id.gif_edit_repeat);
        sRepeat.setOnSeekBarChangeListener(this);

        durationValue = (TextView)view.findViewById(R.id.gif_edit_duration_value);
        qualityValue = (TextView)view.findViewById(R.id.gif_edit_quality_value);
        repeatValue = (TextView)view.findViewById(R.id.gif_edit_repeat_value);

        durationValue.setText(String.valueOf(duration));
        qualityValue.setText(String.valueOf(quality));
        repeatValue.setText(String.valueOf(repeat));
        sDuration.setProgress(duration);
        sQuality.setProgress(quality);
        sRepeat.setProgress(repeat);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        listener.onGifSettingsUpdated(duration, quality, repeat);
        super.onDismiss(dialog);
    }
}
