package com.skaarstein.makinit.share;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;
import com.skaarstein.makinit.gallery.GalleryImage;
import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.edit.ImageInfo;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.GifDecoder;
import com.skaarstein.makinit.utils.GifEncoder;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Martin on 16.02.2016.
 */
public class ShareFragment extends Fragment implements View.OnClickListener, ShareSliderAdapter.SliderAdapterListener, GifSettingsFragment.OnGifSettingsUpdated{

    private static final String TAG = ShareFragment.class.getName();

    private RelativeLayout loadingPanel;
    private ImageView display;
    private ImageView displayControl;
    private RecyclerView slider;
    private ShareSliderAdapter sliderAdapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout imageInfo;
    private TextView imageDate;
    private Uri projectUri;
    private boolean gifGenerated = false;
    private boolean gifChanged = false;
    private int ID;
    private GalleryImage displayImage;
    private int imageToDeletePosition;

    public static ShareFragment newInstance(@NonNull int ID){
        ShareFragment galleryFragment = new ShareFragment();
        galleryFragment.ID = ID;
        return galleryFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAppBar();
    }

    @Override
    public void onStart() {
        super.onStart();
        initAppBar();
    }

    private void initAppBar(){
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.share_title);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_share, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if(savedInstanceState == null) {

            slider = (RecyclerView)view.findViewById(R.id.share_images_slider);
            layoutManager = new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL, false);
            slider.setLayoutManager(layoutManager);

            loadingPanel = (RelativeLayout)view.findViewById(R.id.loadingPanel);
            display = (ImageView)view.findViewById(R.id.display_image);
            display.setOnClickListener(this);
            view.findViewById(R.id.share_btn_adjust).setOnClickListener(this);
            view.findViewById(R.id.share_btn_share).setOnClickListener(this);

            displayControl = (ImageView)view.findViewById(R.id.display_control);
            displayControl.setOnClickListener(this);

            imageInfo = (RelativeLayout)view.findViewById(R.id.display_image_info);
            imageDate = (TextView)view.findViewById(R.id.display_image_captured);

            view.findViewById(R.id.display_image_delete).setOnClickListener(this);

            ShareLoader ShareLoader = new ShareLoader(ImageResolution.P360p);
            ShareLoader.execute(ID);

        }
    }

    @Override
    public void onSlideSelected(int position, boolean clicked) {
        galleryIndex = position;
        if(clicked)pauseGif();
        displayImage = sliderAdapter.getImage(position);
        display.setImageBitmap(displayImage.image);
        imageDate.setText(displayImage.date);
        if(clicked) {
            Log.d(TAG, "Item selected: " + position);
            layoutManager.scrollToPosition(position);
            imageInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSlideDelete(int position) {
        deleteSlide(position);
    }

    @Override
    public void onGifSettingsUpdated(int duration, int quality, int repeat) {
        imageDuration = duration;
        imageQuality = quality;
        imageRepeat = repeat;
        gifChanged = true;
        Log.d(TAG, "Gif settings updated: " + duration + ", " + quality + ", " + repeat);
    }

    private class ShareLoader extends AsyncTask<Integer, Integer, GalleryImage[]>{

        private final ImageResolution resolution;
        private String titleText;
        private String descText;
        private ProjectInfo projectInfo;

        public ShareLoader(ImageResolution resolution) {
            this.resolution = resolution;
        }

        @Override
        protected void onPreExecute() {
            loadingPanel.setVisibility(View.VISIBLE);
        }

        @Override
        protected GalleryImage[] doInBackground(Integer... params) {
            try{
                int prjID = params[0];
                if(loadProject(prjID) == null) return null;

                DatabaseHandler dh = new DatabaseHandler(getActivity());
                final ArrayList<GalleryImage> gallery = new ArrayList<>();

                if(dh.getImageCount(prjID) < 2) return null;

                Log.d(TAG, "Loading images from project: " + prjID);
                dh.loadImages(prjID, new DatabaseHandler.ImageInfoLoader() {
                    @Override
                    public void onImageLoaded(ImageInfo inf, int position) {
                        GalleryImage galleryImage = new GalleryImage(inf.ID,
                                inf.date);
                        galleryImage.title = inf.title;
                        galleryImage.description = inf.description;
                        galleryImage.image = ImageUtils.scaleImage(inf.imageData, resolution);
                        gallery.add(galleryImage);
                    }

                    @Override
                    public void onError(int position, String message) {
                        Log.d(TAG, message + " @ " + position);
                    }
                });

                return gallery.toArray(new GalleryImage[gallery.size()]);
            } catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        private ProjectInfo loadProject(int ID){
            try{
                DatabaseHandler dh = new DatabaseHandler(getActivity());
                ProjectInfo prj = dh.getProject(ID);
                titleText = prj.name;
                descText = prj.description;
                projectInfo = prj;
                return prj;
            } catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(GalleryImage[] images) {
            super.onPostExecute(images);

            if(images == null){
                getFragmentManager().popBackStack();
            }

            sliderAdapter = new ShareSliderAdapter(getActivity(), images, ShareFragment.this);
            slider.setAdapter(sliderAdapter);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(titleText);
            loadingPanel.setVisibility(View.GONE);

            if(images.length == 0){
                getFragmentManager().popBackStack();
            }

            initGifPlayer();
            playGif();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.share_btn_share:{
                if(gifGenerated) {
                    if(gifChanged){
                        GifGenerator gifGenerator = new GifGenerator();
                        gifGenerator.execute(sliderAdapter.getImages());
                    } else {
                        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.setType("image/gif");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, projectUri);
                        getActivity().startActivity(Intent.createChooser(shareIntent, "Share Project"));
                    }
                } else {
                    GifGenerator gifGenerator = new GifGenerator();
                    gifGenerator.execute(sliderAdapter.getImages());
                }
                break;
            }
            case R.id.display_image_delete:{
                deleteSlide(galleryIndex);
                break;
            }
            case R.id.display_image:{
                if(gifIsPlaying){
                    pauseGif();
                    imageInfo.setVisibility(View.VISIBLE);
                }
                else{
                    if(gifPlayerRunning) {
                        resumeGif();
                    } else {
                        initGifPlayer();
                        playGif();
                    }
                    imageInfo.setVisibility(View.GONE);
                }
                break;
            }
            case R.id.share_btn_adjust:
                GifSettingsFragment settingsFragment = GifSettingsFragment.newInstance(this,
                        imageDuration, imageQuality, imageRepeat);
                settingsFragment.show(getFragmentManager(), "GifSettings");
                break;
        }
    }

    private void deleteSlide(int position){
        if(2 == sliderAdapter.getItemCount()){
            Toast.makeText(getActivity(),
                    R.string.share_image_delete_error, Toast.LENGTH_SHORT).show();
            return;
        }

        imageToDeletePosition = position;

        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.share_image_delete_message)
                .setPositiveButton(R.string.share_image_delete_confirm, deleteListener)
                .setNegativeButton(R.string.share_image_delete_decline, deleteListener)
                .show();
    }

    private DialogInterface.OnClickListener deleteListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(DialogInterface.BUTTON_POSITIVE == which) {
                stopGif();
                sliderAdapter.deleteSlide(imageToDeletePosition);
            }
        }
    };

    private class GifGenerator extends AsyncTask<Bitmap[], Integer, Uri>{

        private GifEncoder gifEncoder;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingPanel.setVisibility(View.VISIBLE);
            stopGif();
        }

        @Override
        protected void onPostExecute(Uri gifLocation) {
            super.onPostExecute(gifLocation);
            gifGenerated = true;
            loadingPanel.setVisibility(View.GONE);
            projectUri = gifLocation;

            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("image/gif");
            shareIntent.putExtra(Intent.EXTRA_STREAM, projectUri);
            getActivity().startActivity(Intent.createChooser(shareIntent, "Share Project"));
        }

        protected byte[] generateGif(){
            gifEncoder = new GifEncoder();
            gifEncoder.setDelay(imageDuration);
            gifEncoder.setQuality(imageQuality);
            gifEncoder.setRepeat(imageRepeat);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            gifEncoder.start(bos);
            Bitmap[] images = sliderAdapter.getImages();
            for(Bitmap tempBit : images){
                gifEncoder.addFrame(tempBit);
            }
            gifEncoder.finish();
            Log.d(TAG, "Gif created");

            return bos.toByteArray();
        }

        public File getFile() {
            try {
                File cameraDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DCIM).toString());

                File tmp = new File(cameraDir, String.valueOf(ID) + ".gif");
                if(tmp.exists()){
                    Log.d(TAG, "Deleting old file...");
                    tmp.delete();
                }
                return tmp;
            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        @Override
        protected Uri doInBackground(Bitmap[]... params) {
            try{
                byte[] gifBytes = generateGif();
                File output = getFile();
                Log.d(TAG, "Writing gif to: " + output.getAbsolutePath());
                FileOutputStream outStream = new FileOutputStream(output);
                outStream.write(gifBytes);
                outStream.close();
                Log.d(TAG, "Gif saved");
                return Uri.fromFile(output);
            }catch(Exception e){
                e.printStackTrace();
                Log.e(TAG, e.getMessage());
                return null;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopGif();
    }

    private boolean gifIsPlaying = false;
    private boolean gifPlayerRunning = false;
    private int galleryIndex = 0;
    private int galleryCount = 0;
    private GifDecoder gifDecoder;

    private boolean scrollToPosition = false;
    private int galleryItemSize = 0;
    private int sliderWidth = 0;
    private int sliderThreshold = 0;
    private int pointerDistance = 0;
    private int pointerSpeed = 0;
    private int imageDuration = 300;
    private int imageQuality = 30;
    private int imageRepeat = 0;

    final Handler mHandler = new Handler();

    final Runnable updateResults = new Runnable() {
        public void run() {
            sliderAdapter.selectItem(galleryIndex);
            if(scrollToPosition) layoutManager.scrollToPosition(galleryIndex);
        }
    };

    private void pauseGif(){
        if(gifIsPlaying){
            gifIsPlaying = false;
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.display_control);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    displayControl.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            displayControl.setVisibility(View.VISIBLE);
            displayControl.setImageDrawable(getActivity().getDrawable(R.drawable.ic_pause_white_24dp));
            displayControl.startAnimation(animation);
        }
    }
    private void stopGif(){
        if(gifPlayerRunning) {
            gifPlayerRunning = false;
            pauseGif();
        }
    }
    private void resumeGif(){
        if(!gifIsPlaying){
            gifIsPlaying = true;
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.display_control);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    displayControl.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            displayControl.setVisibility(View.VISIBLE);
            displayControl.setImageDrawable(getActivity().getDrawable(R.drawable.ic_play_arrow_white_24dp));
            displayControl.startAnimation(animation);
        }
    }
    private void initGifPlayer(){

        sliderWidth = slider.getWidth();
        galleryCount = sliderAdapter.getItemCount();
        galleryItemSize = sliderAdapter.getSlideSize();
        sliderThreshold = sliderWidth / 2;
        pointerDistance = layoutManager.getWidth();
        pointerSpeed = imageDuration * galleryCount;

        Log.d(TAG, "GifPlayer: " + galleryCount + " gallerysize, " + sliderThreshold
                + " threshold, " + pointerDistance + " pointerDistance, "
                + pointerSpeed + " pointerSpeed");
    }

    private void playGif() {

        gifPlayerRunning = true;
        resumeGif();

        new Thread(new Runnable() {
            public void run() {
                while (gifPlayerRunning) {
                    while (gifIsPlaying) {
                        while (galleryIndex < galleryCount){
                            if(!gifIsPlaying)break;
                            mHandler.post(updateResults);
                            try {
                                Thread.sleep(imageDuration);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            galleryIndex += 1;
                        }
                        if(gifIsPlaying)galleryIndex = 0;
                    }
                }
            }
        }).start();
    }

    private void playGif(final InputStream movieStream) {
        resumeGif();
        gifPlayerRunning = true;

        gifDecoder = new GifDecoder();
        gifDecoder.read(movieStream);

        new Thread(new Runnable() {
            public void run() {
                final int n = gifDecoder.getFrameCount();
                final int ntimes = gifDecoder.getLoopCount();

                while (gifPlayerRunning) {
                    int repetitionCounter = 0;
                    while (gifIsPlaying) {
                        for (int i = 0; i < n; i++) {
                            final int t = gifDecoder.getDelay(i);
                            mHandler.post(updateResults);
                            try {
                                Thread.sleep(t);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if(ntimes != 0) {
                            repetitionCounter ++;
                        }
                    }
                }
            }
        }).start();
    }
}
