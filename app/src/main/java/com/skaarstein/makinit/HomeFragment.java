package com.skaarstein.makinit;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.melnykov.fab.FloatingActionButton;
import com.skaarstein.makinit.edit.ProjectFragment;
import com.skaarstein.makinit.utils.DatabaseHandler;

/**
 * Created by Martin on 16.02.2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, ProjectFragment.OnDataSetChangeListener{

    private static final String TAG = HomeFragment.class.getName();

    private ListView projectView;
    private ProjectViewAdapter adapter;
    private RelativeLayout loadingPanel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View root;


    public static HomeFragment newInstance(){
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    private void initAppBar(){
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.home_title);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        initAppBar();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        root.setClickable(true);

        if(savedInstanceState == null) {
            projectView = (ListView) view.findViewById(R.id.project_view);
            loadingPanel = (RelativeLayout) view.findViewById(R.id.loadingPanel);

            swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);

            DatabaseHandler dh = new DatabaseHandler(getActivity());
            adapter = new ProjectViewAdapter(getActivity(), dh.getProjectCursor());
            projectView.setAdapter(adapter);
            dh.close();

            projectView.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int topRowVerticalPosition =
                            (projectView == null || projectView.getChildCount() == 0) ?
                                    0 : projectView.getChildAt(0).getTop();
                    if (firstVisibleItem == 0 && topRowVerticalPosition >= 0) {
                        swipeRefreshLayout.setEnabled(true);
                        DatabaseHandler dh = new DatabaseHandler(getActivity());
                        adapter.swapCursor(dh.getProjectCursor());
                        dh.close();
                        swipeRefreshLayout.setRefreshing(true);
                    } else swipeRefreshLayout.setEnabled(false);

                }
            });

            FloatingActionButton fab = (FloatingActionButton)view.findViewById(R.id.fab);
            fab.setOnClickListener(this);
            fab.attachToListView(projectView);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        onDataChanged();
        initAppBar();
    }

    @Override
    public void onClick(View v) {
        root.setClickable(false);
        ProjectFragment.newInstance(-1, this).show(getFragmentManager(), "Create");
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);
    }


    @Override
    public void onDataChanged() {
        try {
            DatabaseHandler dh = new DatabaseHandler(getActivity());
            adapter.swapCursor(dh.getProjectCursor());
            dh.close();
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }
}
