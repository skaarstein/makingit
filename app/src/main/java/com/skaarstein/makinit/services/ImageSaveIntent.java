package com.skaarstein.makinit.services;

import android.app.Application;
import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.*;
import android.util.Log;
import android.widget.Toast;

import com.skaarstein.makinit.edit.ImageInfo;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ExifUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Martin on 10.04.2016.
 */
public class ImageSaveIntent extends IntentService {

    public static final String PROJECT_ID = "project_id";
    public static final String IS_FRONT_CAMERA = "is_front_camera";
    public static final String IMAGE_ABSOLUTE_PATH = "image_url";
    public static final String DELETE_TMP_IMAGE = "delete_tmp_image";
    public static final String IMAGE_DESCRIPTION = "image_description";
    public static final String IMAGE_TITLE = "image_title";
    public static final String IMAGE_SAVE_TO_GALLERY = "save_to_gallery";
    public static final String BROADCAST_TYPE = "image_notify";
    public static final String BROADCAST_ACTION = "database_action";

    private static final String TAG = ImageSaveIntent.class.getName();


    public ImageSaveIntent() {
        super(TAG);
    }

    private int generateKey() {
        int uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        return uniqueId;
    }

    private void error(){
        showToast("Error while saving image");
    }

    private void success(){
        showToast("Image saved");

        Intent intent = new Intent();
        intent.setAction(BROADCAST_ACTION);
        intent.setType(BROADCAST_TYPE);
        sendBroadcast(intent);
    }

    public void showToast(String message) {
        final String msg = message;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int prjID = intent.getIntExtra(PROJECT_ID, -1);

        if(prjID == -1){
            error();
            return;
        }

        boolean isFrontCamera = intent.getBooleanExtra(IS_FRONT_CAMERA, false);
        String tmpPath = intent.getStringExtra(IMAGE_ABSOLUTE_PATH);

        if(tmpPath == null){
            error();
            return;
        }

        boolean delTmpFile = intent.getBooleanExtra(DELETE_TMP_IMAGE, true);

        File inputFile = new File(tmpPath);
        Bitmap tempBitmap = BitmapFactory.decodeFile(inputFile.getAbsolutePath());
        Bitmap finalImage = ExifUtil.rotateBitmap(inputFile.getAbsolutePath(),
                tempBitmap);
        
        if(delTmpFile)inputFile.delete();

        Application app = getApplication();
        DatabaseHandler dh = new DatabaseHandler(app);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        int imageID = generateKey();
        ImageInfo img = new ImageInfo(imageID, prjID, sdf.format(new Date()));

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        FileOutputStream output = null;
        File outputFile = null;

        try {

            finalImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] data = stream.toByteArray();

            img.title = intent.getStringExtra(IMAGE_TITLE);
            img.description = intent.getStringExtra(IMAGE_DESCRIPTION);
            img.imageData = data;

            Log.d(TAG, data.length + ", " + data.toString());

            dh.addImage(img);

            ProjectInfo prj = dh.getProject(prjID);
            prj.cover = img.ID;
            dh.updateProject(prj);

            if(intent.getBooleanExtra(IMAGE_SAVE_TO_GALLERY, false)) {

                File cameraDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM).toString() + "/MakingIt");

                if (!cameraDir.exists()) {
                    cameraDir.mkdirs();
                }

                outputFile = File.createTempFile(imageID + "", "jpg", cameraDir);

                output = new FileOutputStream(outputFile);
                output.write(data);
            }

            success();

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            error();
        }finally {
            try{
                stream.close();
                if(output != null) output.close();
            }catch (IOException ex){
                Log.e(TAG, ex.getMessage());
            }
        }
    }
}
