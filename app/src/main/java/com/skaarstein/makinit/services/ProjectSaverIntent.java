package com.skaarstein.makinit.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.util.Log;
import android.widget.Toast;

import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.utils.DatabaseHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Martin on 11.04.2016.
 */
public class ProjectSaverIntent extends IntentService {

    public static final String PROJECT_NAME = "project_name";
    public static final String PROJECT_DESCRIPTION = "project_desc";
    public static final String PROJECT_COVER_ID = "project_cover";
    public static final String PROJECT_ID = "project_id";
    public static final String NOTIFY = "notify";

    private static final String TAG = ProjectSaverIntent.class.getName();

    public ProjectSaverIntent() {
        super(TAG);
    }

    private int generateKey(){
        int uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        return uniqueId;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

            boolean createNew = false;
            boolean notify = intent.getBooleanExtra(NOTIFY, true);

            ProjectInfo projectInfo;
            SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/yyyy");
            String date = sdf.format(new Date());

            final int ID = intent.getIntExtra(PROJECT_ID, -1);

            if(ID != -1){
                projectInfo = new ProjectInfo(ID);
            }else {
                projectInfo = new ProjectInfo(generateKey(), date);
                createNew = true;
            }

            final String prjName = intent.getStringExtra(PROJECT_NAME);
            final String prjDesc = intent.getStringExtra(PROJECT_DESCRIPTION);
            if(prjName != null) projectInfo.name = prjName;
            if(prjDesc != null) projectInfo.description = prjDesc;
            projectInfo.cover = intent.getIntExtra(PROJECT_COVER_ID, -1);

            DatabaseHandler dh = new DatabaseHandler(getApplication());

            if(createNew){
                dh.addProject(projectInfo);
                if(notify)showToast("Project created");
            }
            else {
                dh.updateProject(projectInfo);
                if (notify)showToast("Project updated");
            }

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    public void showToast(String message) {
        final String msg = message;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showToast(final @StringRes int id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
