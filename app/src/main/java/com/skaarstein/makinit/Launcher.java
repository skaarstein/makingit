package com.skaarstein.makinit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.VideoView;

import java.io.IOException;

public class Launcher extends Activity implements SurfaceHolder.Callback, MediaPlayer.OnCompletionListener{

    private MediaPlayer mp;
    private SurfaceView preview;
    private SurfaceHolder holder;

    private static final String LOADING = "loading";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        getWindow().setFormat(PixelFormat.UNKNOWN);
        preview = (SurfaceView)findViewById(R.id.launcher_video_surface);
        holder = preview.getHolder();
        holder.addCallback(this);
        mp = new MediaPlayer();
        mp.setOnCompletionListener(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        mp.setDisplay(holder);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        if(!preferences.getBoolean(LOADING, false)){
            play(Uri.parse("android.resource://com.skaarstein.makinit/raw/skaarstein"));
            preferences.edit().putBoolean(LOADING, true).commit();
        }else {
            play(Uri.parse("android.resource://com.skaarstein.makinit/raw/makingit"));
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }
    void play(Uri media) {
        try {
            mp.setDataSource(this, media);
            mp.prepare();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mp.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Intent mainIntent = new Intent(Launcher.this, Home.class);
        startActivity(mainIntent);
        finish();
    }
}
