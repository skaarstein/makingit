package com.skaarstein.makinit.gallery;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by SlyOtis on 19.02.2016.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private static final String TAG = GalleryAdapter.class.getName();
    private Context context;
    private int ID;
    private Cursor cursor;
    private int lastSelected = -1;

    private Hashtable<Integer, OnGalleryItemSelected> galleryItems;
    private ArrayList<OnGalleryItemSelected> onGalleryItemSelectedListeners;


    public GalleryAdapter(Context context, int ID, OnGalleryItemSelected listener) {
        this.context = context;
        this.ID = ID;
        this.galleryItems = new Hashtable<>();
        this.onGalleryItemSelectedListeners = new ArrayList<>();
        CursorLoader cursorLoader = new CursorLoader();
        cursorLoader.execute(ID);
        if(listener != null)
            onGalleryItemSelectedListeners.add(listener);
    }

    public interface OnGalleryItemSelected{
        void onItemSelected(int position);
        void onItemDeselected(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, OnGalleryItemSelected {
        // each data item is just a string in this case
        public ImageView display;
        private View overlay;
        public int position = -1;
        public int imageInfoID;

        public ViewHolder(View view) {
            super(view);
            display = (ImageView)view.findViewById(R.id.gallery_item_display);
            overlay = view.findViewById(R.id.gallery_item_selected);
        }

        @Override
        public void onItemSelected(int position) {
            overlay.setVisibility(View.VISIBLE);
        }

        @Override
        public void onItemDeselected(int position) {
            overlay.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            selectItem(position);
        }
    }

    private void selectItem(int position){
        if(lastSelected > 0){
            galleryItems.get(lastSelected).onItemDeselected(lastSelected);
        }
        galleryItems.get(position).onItemSelected(position);

        for(OnGalleryItemSelected l : onGalleryItemSelectedListeners){
            l.onItemSelected(position);
            l.onItemDeselected(lastSelected);
        }

        lastSelected = position;
    }

    public void addOnGalleryItemSelectedListener(OnGalleryItemSelected listener){
        onGalleryItemSelectedListeners.add(listener);
    }

    public void removeOnGalleryItemSelectedListener(OnGalleryItemSelected listener){
        onGalleryItemSelectedListeners.remove(listener);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.galllery_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try{
            holder.imageInfoID = cursor.getInt(0);
            holder.display.setImageBitmap(
                    ImageUtils.scaleImage(cursor.getBlob(2), ImageResolution.P360p));

            if(holder.position > 0){
                holder.position = position;
                galleryItems.put(position, holder);
            } else {
                galleryItems.remove(holder.position);
                holder.position = position;
                galleryItems.put(position, holder);
            }

            if(cursor.isLast()) cursor.moveToFirst();
            else cursor.moveToNext();
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        if(cursor != null)
            return cursor.getCount();
        return 0;
    }

    private class CursorLoader extends AsyncTask<Integer, Integer, Cursor>{
        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            GalleryAdapter.this.cursor = cursor;
            cursor.moveToFirst();
            notifyDataSetChanged();
        }

        @Override
        protected Cursor doInBackground(Integer... params) {
            DatabaseHandler dh = new DatabaseHandler(context);
            return dh.getImageDataCursor(params[0]);
        }
    }
}
