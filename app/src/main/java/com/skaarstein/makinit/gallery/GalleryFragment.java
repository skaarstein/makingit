package com.skaarstein.makinit.gallery;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.HomeFragment;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ExifUtil;
import com.skaarstein.makinit.utils.Serializer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created by Martin on 16.02.2016.
 */
public class GalleryFragment extends Fragment implements GalleryAdapter.OnGalleryItemSelected{

    private static final String TAG = GalleryFragment.class.getName();
    private RelativeLayout loadingPanel;
    private RecyclerView gallery;
    private ProjectInfo projectInfo;
    private FloatingActionButton actionButton;
    private ImageView display;
    private Bitmap[] images;
    private int ID;

    public static GalleryFragment newInstance(@NonNull int ID){
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.ID = ID;
        return galleryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if(savedInstanceState == null) {
            gallery = (RecyclerView) view.findViewById(R.id.gallery_slider);
            setListLayoutManager();
            gallery.setAdapter(new GalleryAdapter(getActivity(), ID, this));
            gallery.addOnScrollListener(onScrollListener);

            loadingPanel = (RelativeLayout)view.findViewById(R.id.loadingPanel);
            display = (ImageView)view.findViewById(R.id.display_image);
        }

    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };

    private void setListLayoutManager(){
        GridLayoutManager layoutManager
                = new GridLayoutManager(getActivity(), 4, GridLayoutManager.VERTICAL, false);
        gallery.setLayoutManager(layoutManager);
        gallery.requestLayout();
    }

    @Override
    public void onItemSelected(int position) {
        Log.d(TAG, position + " is selected");
    }

    @Override
    public void onItemDeselected(int position) {
        Log.d(TAG, position + " were deselected");
    }
}
