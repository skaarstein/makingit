package com.skaarstein.makinit.gallery;

import android.graphics.Bitmap;

/**
 * Created by pc on 07.03.2016.
 */
public class GalleryImage {
    public Bitmap image;
    public final int ID;
    public final String date;
    public String title;
    public String description;

    public GalleryImage(int ID, String date) {
        this.ID = ID;
        this.date = date;
    }
}