package com.skaarstein.makinit.edit;

import java.io.Serializable;

/**
 * Created by Martin Ottersland on 3/3/2016.
 */
public class ImageInfo implements Serializable{
    public final int ID;
    public final int projectID;
    public final String date;
    public String title;
    public String description;
    public byte[] imageData;

    /**
     * Returns a new instance for creating a completely new image to
     * add into the database.
     * @param ID  image id.
     * @param projectID id of parent project.
     * @param date creation date in format DD/MM/yyyy.
     */
    public ImageInfo(int ID, int projectID, String date) {
        this.ID = ID;
        this.projectID = projectID;
        this.date = date;
    }

    /**
     * Returns an instance without info about image creation date etc..
     * Used for updating the image database
     * @param ID of the image to update values for
     */
    public ImageInfo(int ID) {
        this.ID = ID;
        this.projectID = 0;
        this.date = null;
        this.imageData = null;
        this.title = null;
        this.description = null;
    }
}
