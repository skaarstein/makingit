package com.skaarstein.makinit.edit;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.camera.CameraActivity;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Martin on 12.02.2016.
 */
public class ProjectFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = ProjectFragment.class.getName();
    private ProjectInfo projectInfo;
    private RelativeLayout projectContainer;
    private EditText title;
    private EditText description;
    private ImageView cover;

    private OnDataSetChangeListener listener;
    private int ID;
    private boolean createNew;

    public static ProjectFragment newInstance(int ID, OnDataSetChangeListener listener){
        ProjectFragment projectFragment = new ProjectFragment();
        projectFragment.ID = ID;
        projectFragment.listener = listener;
        return projectFragment;
    }

    public interface OnDataSetChangeListener{
        void onDataChanged();
    }

    private int generateKey(){
        int uniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        return uniqueId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null) {

            TextView complete = (TextView)view.findViewById(R.id.edit_info_complete);
            complete.setOnClickListener(this);
            projectContainer = (RelativeLayout) view.findViewById(R.id.edit_info_container);
            projectContainer.setOnClickListener(this);
            title = (EditText) view.findViewById(R.id.edit_info_title);
            description = (EditText) view.findViewById(R.id.edit_info_description);
            cover = (ImageView) view.findViewById(R.id.edit_info_cover);

            cover.setOnClickListener(this);

            if (ID == -1) {
                ID = generateKey();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                projectInfo = new ProjectInfo(ID, sdf.format(new Date()));
                complete.setText(R.string.edit_info_prj_new_complete);
                createNew = true;
            } else {
                createNew = false;
                complete.setText(R.string.edit_info_prj_edit_complete);
                ProjectLoader pl = new ProjectLoader(ImageResolution.P480p);
                pl.execute();
            }
        }


    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if(dialog != null) {
            dialog.setCanceledOnTouchOutside(true);
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.project_container_size));
            window.setGravity(Gravity.BOTTOM);
        }
    }

    private class ProjectLoader extends AsyncTask<String, Integer, ProjectInfo>{

        private Bitmap coverImage = null;
        private final ImageResolution resolution;

        public ProjectLoader(ImageResolution resolution) {
            this.resolution = resolution;
        }

        @Override
        protected ProjectInfo doInBackground(String... params) {
            try {
                Log.d(TAG, "Loading project...");
                DatabaseHandler dh = new DatabaseHandler(getActivity());
                ProjectInfo prj = dh.getProject(ID);

                if(prj.cover != 0) {
                    byte[] data = dh.getImageData(prj.cover);
                    coverImage = ImageUtils.scaleImage(data, resolution);
                }

                return prj;
            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(ProjectInfo prj) {
            super.onPostExecute(prj);

            if(coverImage != null)
                cover.setImageBitmap(coverImage);

            title.setText(prj.name);
            description.setText(prj.description);
            projectInfo = prj;
        }
    }

    private class ProjectSaver extends AsyncTask<String, Integer, Boolean>{

        private String titleText;
        private String descText;
        private final boolean showCamera;

        public ProjectSaver(boolean showCamera) {
            this.showCamera = showCamera;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            titleText = title.getText().toString();
            descText = description.getText().toString();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/yyyy");
                projectInfo.updated = sdf.format(new Date());
                projectInfo.description = descText;
                projectInfo.name = titleText;

                DatabaseHandler dh = new DatabaseHandler(getActivity());

                if(createNew) dh.addProject(projectInfo);
                else dh.updateProject(projectInfo);

            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            ProjectFragment.this.dismiss();
            if(showCamera){
                Intent intent = new Intent(getActivity(), CameraActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Home.ID_KEY, ID);
                getActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        listener.onDataChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_info_complete:{
                ProjectSaver ps = new ProjectSaver(false);
                ps.execute();
                break;
            }
            case R.id.edit_info_cover:{
                ProjectSaver ps = new ProjectSaver(true);
                ps.execute();
                break;
            }
            default:
                break;
        }
    }
}
