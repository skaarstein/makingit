package com.skaarstein.makinit.edit;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.skaarstein.makinit.R;

/**
 * Created by Martin on 13.03.2016.
 */
public class ImageFragment extends DialogFragment implements View.OnClickListener{
    private static final String TAG = ImageFragment.class.getName();
    private EditText title;
    private EditText description;
    private Bitmap image;
    private String oldTitle;
    private String oldDesc;

    private OnInfoUpdatedListener listener;

    public static ImageFragment newInstance(Bitmap image, String oldTitle,
                                            String oldDesc, OnInfoUpdatedListener listener){
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.image = image;
        imageFragment.listener = listener;
        imageFragment.oldTitle = oldTitle;
        imageFragment.oldDesc = oldDesc;
        return imageFragment;
    }

    public interface OnInfoUpdatedListener{
        void onInfoUpdated(String title, String description);
        void onInfoEditClose();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null) {

            TextView complete = (TextView)view.findViewById(R.id.edit_info_complete);
            complete.setOnClickListener(this);
            complete.setText(getActivity().getResources().getString(R.string.edit_info_image_complete));
            view.findViewById(R.id.edit_info_capture).setVisibility(View.GONE);
            title = (EditText) view.findViewById(R.id.edit_info_title);
            description = (EditText) view.findViewById(R.id.edit_info_description);
            title.setHint(R.string.edit_info_img_title_hint);
            description.setHint(R.string.edit_info_img_desc_hint);
            ((ImageView) view.findViewById(R.id.edit_info_cover)).setImageBitmap(image);

            if(oldTitle != null) title.setText(oldTitle);
            if(oldDesc != null) description.setText(oldDesc);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if(dialog != null) {
            dialog.setCanceledOnTouchOutside(true);
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) getResources().getDimension(R.dimen.project_container_size));
            window.setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.edit_info_complete:{
                listener.onInfoEditClose();
                dismiss();
            }
            default:{
                listener.onInfoUpdated(title.getText().toString(), description.getText().toString());
            }
        }

    }
}
