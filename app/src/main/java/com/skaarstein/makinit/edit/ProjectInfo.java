package com.skaarstein.makinit.edit;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.security.PublicKey;

/**
 * Created by SlyOtis on 01.02.2016.
 */
public class ProjectInfo implements Serializable{
    public final int ID;
    public final String created;
    public String name;
    public String description;
    public String updated;
    public int cover;

    public ProjectInfo(@NonNull int ID, @NonNull String created) {
        this.ID = ID;
        this.created = created;
        this.cover = 0;
    }

    public ProjectInfo(@NonNull int ID) {
        this.ID = ID;
        this.created = null;
        this.cover = 0;
    }
}
