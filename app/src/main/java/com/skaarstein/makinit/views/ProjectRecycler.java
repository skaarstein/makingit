package com.skaarstein.makinit.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Martin on 25.02.2016.
 */
public class ProjectRecycler extends RecyclerView {
    public ProjectRecycler(Context context) {
        super(context);
    }

    public ProjectRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProjectRecycler(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public boolean canScrollVertically(int direction) {
        // check if scrolling up
        if (direction < 1) {
            boolean original = super.canScrollVertically(direction);
            return !original && getChildAt(0) != null && getChildAt(0).getTop() < 0 || original;
        }
        return super.canScrollVertically(direction);

    }
}
