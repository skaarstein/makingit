package com.skaarstein.makinit.views;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.LayoutDirection;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.AdjustmentDrawerAnimator;

/**
 * Created by Martin on 15.04.2016.
 */
public class AdjustmentDrawer extends FrameLayout implements View.OnClickListener{

    private static final String TAG = AdjustmentDrawer.class.getName();

    public AdjustmentDrawer(Context context) {
        super(context);
        init(context);
    }

    public AdjustmentDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AdjustmentDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private Context context;
    private AdjustmentDrawerAnimator animator;

    private void init(Context context){
        this.context = context;
        int itemSize = (int)context.getResources().getDimension(R.dimen.camera_btn_small);

        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width = AdjustmentDrawer.this.getWidth() * (2/3);
                animator = new AdjustmentDrawerAnimator(itemSize, itemSize, itemSize, itemSize,
                        width, new RectF(0, 0, AdjustmentDrawer.this.getWidth(),
                        AdjustmentDrawer.this.getHeight()), new PointF())
            }
        });
        restoreDefaultLayout();
        this.invalidate();
    }

    public void setDrawerMargin(float layoutMargin){
        this.mLayoutMargin = layoutMargin;
    }

    public void setMaxItems(int maxItems){
        this.mMaxItems = maxItems;
    }

    public void setDuration(int rotDuration, int transDuration, int alphaDuration){
        this.mRotDuration = rotDuration;
        this.mTransDuration = transDuration;
        this.mAlphaDuration = alphaDuration;
    }

    public void setDelay(int delay){
        this.mDelay = delay;
    }

    public void setItemSize(float size){
        this.mItemSize = size;
    }

    public void rotateDrawer(int rotation){
        int count = container.getChildCount();
        int height = this.getHeight();
        int width = this.getWidth();
        ViewPropertyAnimator anim = container.animate()
                .rotation(rotation)
                .setInterpolator(mRotPolator)
                .setDuration(mRotDuration);
        switch (rotation){
            case 0:{
                mStartPosition = count * mItemSize;
                anim.x(0).y(height - container.getHeight() - mLayoutMargin);
                updateLayout(LayoutDirection.LTR);
                break;
            }
            case 90:{
                mStartPosition = count * mItemSize;
                anim.x(width - container.getHeight()).y(height - container.getWidth() - mLayoutMargin);
                updateLayout(LayoutDirection.RTL);
                break;
            }
            case 180:{
                mStartPosition = 0;
                anim.x(width).y(height - mLayoutMargin);
                updateLayout(LayoutDirection.RTL);
                break;
            }
            case 270:{
                mStartPosition = 0;
                anim.x(0).y(height - mLayoutMargin);
                updateLayout(LayoutDirection.LTR);
                break;
            }
        }
        anim.start();
        container.invalidate();
    }

    private void updateLayout(int direction){
        int count = container.getChildCount();
        for(int i = 0; i < count; i++){
            LinearLayout v = (LinearLayout)container.getChildAt(i);
            v.getChildAt(1).setPivotX(direction == LayoutDirection.RTL
                    ? v.getWidth() + (mItemSize/2) : - mItemSize/2);
            v.setLayoutDirection(direction);
            v.invalidate();
        }
        this.invalidate();
    }

    public void setAdjustmentDrawerListener(AdjustmentDrawerListener listener){
        this.listener = listener;
    }

    public void setInterpolator(TimeInterpolator rotPolator, TimeInterpolator transPolator,
                                TimeInterpolator alphaPolator){
        this.mRotPolator = rotPolator;
        this.mTransPolator = transPolator;
        this.mAlphaPolator = alphaPolator;
    }

    public boolean isDrawerOpen(){
        return mDrawerOpen;
    }

    public void updateItemValue(DrawerItem item, int value){
        switch (item){
            case WHITE_BALANCE:{
                this.mWhiteBalance = value;
                break;
            }
            case OVERLAY_OPACITY:{
                this.mOpacity = value;
            }
            case EXPOSURE:{
                this.mExposure = value;
            }
        }
    }

    public void updateItemValue(DrawerItem item, boolean value){

    }

    public void openDrawer(){
        this.setVisibility(VISIBLE);
        if(mItemSelected){
            final ViewGroup parent = (ViewGroup)container.getChildAt(mSelectedPosition);
            parent.getChildAt(2).setVisibility(GONE);
            parent.getChildAt(1).setVisibility(VISIBLE);
            mItemSelected = false;
        }
        this.mDrawerOpen = true;
        int count = container.getChildCount();
        Log.d(TAG, count + " nr of items");
        for(int i = 0; i < count; i++){
            View parent = container.getChildAt(i);
            parent.setVisibility(VISIBLE);

            final View text = parent.findViewById(R.id.adjust_text);
            final int delay = (count - i) * mDelay;

            parent.animate()
                    .setInterpolator(mAlphaPolator)
                    .setDuration(mAlphaDuration + delay)
                    .setStartDelay(delay)
                    .alpha(1f)
                    .start();

            parent.animate()
                    .setDuration(mTransDuration + delay)
                    .setInterpolator(mTransPolator)
                    .y(mItemSize * i)
                    .setStartDelay(delay)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            if (mDrawerOpen) {
                                text.animate()
                                        .setDuration(mTransDuration)
                                        .setInterpolator(mTransPolator)
                                        .x(mItemSize)
                                        .start();
                                text.animate()
                                        .setDuration(mAlphaDuration)
                                        .setInterpolator(mAlphaPolator)
                                        .alpha(1f)
                                        .start();
                            }
                        }
                    }).start();
        }
    }

    public void closeDrawer(){
        this.mDrawerOpen = false;
        final int count = container.getChildCount();
        if(mItemSelected){
            final ViewGroup parent = (ViewGroup)container.getChildAt(mSelectedPosition);
            final View v = parent.getChildAt(2);
            final View info = parent.getChildAt(1);
            v.animate()
                    .setInterpolator(mAlphaPolator)
                    .setDuration(mTransDuration)
                    .alpha(0f)
                    .start();
            v.animate()
                    .setDuration(mTransDuration)
                    .setInterpolator(mTransPolator)
                    .x(-mItemSize)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            if (!mDrawerOpen) {
                                parent.animate()
                                        .setDuration(mTransDuration)
                                        .setInterpolator(mTransPolator)
                                        .y(mStartPosition)
                                        .withEndAction(new Runnable() {
                                            @Override
                                            public void run() {
                                                mItemSelected = false;
                                                v.setVisibility(GONE);
                                                info.setVisibility(VISIBLE);
                                            }
                                        })
                                        .start();

                                parent.animate()
                                        .setInterpolator(mAlphaPolator)
                                        .setDuration(mAlphaDuration)
                                        .alpha(0f)
                                        .start();
                            }
                        }
                    }).start();
        } else {
            for (int i = 0; i < count; i++) {
                final View parent = container.getChildAt(i);
                final View text = parent.findViewById(R.id.adjust_text);
                final int delay = i * mDelay;
                text.animate()
                        .setInterpolator(mAlphaPolator)
                        .setDuration(mAlphaDuration + delay)
                        .setStartDelay(delay)
                        .alpha(0f)
                        .start();

                text.animate()
                        .setDuration(mTransDuration + delay)
                        .setInterpolator(mTransPolator)
                        .setStartDelay(delay)
                        .x(-mItemSize/2)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                if (!mDrawerOpen) {
                                    parent.animate()
                                            .setDuration(mTransDuration)
                                            .setInterpolator(mTransPolator)
                                            .y(mStartPosition)
                                            .start();

                                    parent.animate()
                                            .setInterpolator(mAlphaPolator)
                                            .setDuration(mAlphaDuration)
                                            .setStartDelay(delay)
                                            .alpha(0f)
                                            .start();
                                }
                            }
                        })
                        .start();

            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id < 4) {
            DrawerItem item = DrawerItem.values()[v.getId()];
            switch (item) {
                case EXPOSURE: {
                    Log.d(TAG, "Exposure clicked");
                    return;
                }
                case OVERLAY_OPACITY: {
                    if (!mItemSelected) adjustOpacity();
                    else deselectItem(mSelectedPosition);
                    return;
                }
                case WHITE_BALANCE: {
                    Log.d(TAG, "White balance clicked");
                    return;
                }
                case EDIT: {
                    Log.d(TAG, "Edit clicked");
                    // edit fragment
                    return;
                }
            }
        }else {
            if (isDrawerOpen()) closeDrawer();
        }
    }

    private void adjustOpacity(){
        final int position = items.get(DrawerItem.OVERLAY_OPACITY.getValue(), -1);
        if(position != -1 ){
            SeekBar seekBar = selectItem(position);
            seekBar.setProgress(mOpacity);
        }
    }

    private <T> T selectItem(final int exceptPosition){
        mItemSelected = true;
        mSelectedPosition = exceptPosition;
        final int count = container.getChildCount();
        T content = null;
        for(int i = 0; i < count; i++){
            final ViewGroup parent = (ViewGroup) container.getChildAt(i);
            final View text = parent.findViewById(R.id.adjust_text);
            if (i != exceptPosition) {
                text.animate()
                        .setDuration(mTransDuration)
                        .setInterpolator(mTransPolator)
                        .x(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                if (mDrawerOpen && mItemSelected) {
                                    parent.animate()
                                            .setDuration(mTransDuration)
                                            .setInterpolator(mTransPolator)
                                            .y(exceptPosition * mItemSize)
                                            .start();

                                    parent.animate()
                                            .setInterpolator(mAlphaPolator)
                                            .setDuration(mAlphaDuration)
                                            .alpha(0f)
                                            .withEndAction(new Runnable() {
                                                @Override
                                                public void run() {
                                                    parent.setVisibility(INVISIBLE);
                                                }
                                            })
                                            .start();
                                }
                            }
                        })
                        .start();
            } else {
                final View v = parent.getChildAt(2);
                content = (T)parent.getChildAt(2);
                text.animate()
                        .setDuration(mTransDuration)
                        .setInterpolator(mTransPolator)
                        .x(-mItemSize)
                        .start();
                text.animate()
                        .setDuration(mAlphaDuration)
                        .setInterpolator(mAlphaPolator)
                        .alpha(0f)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                if (mDrawerOpen && mItemSelected) {
                                    text.setVisibility(GONE);
                                    v.setVisibility(VISIBLE);
                                    v.animate()
                                            .alpha(1f)
                                            .setDuration(mAlphaDuration)
                                            .setInterpolator(mAlphaPolator)
                                            .start();
                                }
                            }
                        })
                        .start();
            }
        }
        return content;
    }

    private void deselectItem(final int exceptPosition){
        mItemSelected = false;
        final int count = container.getChildCount();
        for(int i = 0; i < count; i++){
            final ViewGroup parent = (ViewGroup)container.getChildAt(i);
            final View text = parent.findViewById(R.id.adjust_text);
            if(i != exceptPosition){
                parent.setVisibility(VISIBLE);
                parent.animate()
                        .setInterpolator(mAlphaPolator)
                        .setDuration(mAlphaDuration)
                        .alpha(1f)
                        .start();

                parent.animate()
                        .setDuration(mTransDuration)
                        .setInterpolator(mTransPolator)
                        .y(i * mItemSize)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                if (!mItemSelected && mDrawerOpen) {
                                    text.animate()
                                            .setDuration(mRotDuration)
                                            .setInterpolator(mRotPolator)
                                            .rotationY(0f)
                                            .start();
                                }
                            }
                        })
                        .start();

            } else {
                final View v = parent.getChildAt(2);
                v.animate()
                        .alpha(0f)
                        .setDuration(mAlphaDuration)
                        .setInterpolator(mAlphaPolator)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                v.setVisibility(GONE);
                                text.setVisibility(VISIBLE);
                                if(mDrawerOpen && !mItemSelected) {
                                    text.animate()
                                            .setDuration(mRotDuration)
                                            .setInterpolator(mRotPolator)
                                            .rotationY(0f)
                                            .start();
                                }
                            }
                        })
                        .start();

            }
        }
    }


    public enum DrawerItem{
        OVERLAY_OPACITY(0),
        EDIT(1),
        EXPOSURE(2),
        WHITE_BALANCE(3);

        private int value;
        DrawerItem(int value){
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private static final String ITEM_ALREADY_EXIST = "Item already exist in drawer";
    private static final String MAX_ITEMS_REACHED = "Drawer contains maximum number of items";

    public void addItem(DrawerItem item) throws AdjustmentDrawerException{
        int count = container.getChildCount();
        if(count != mMaxItems){
            if(items.get(item.getValue(), -1) == -1){
                items.append(item.getValue(), count);
                instantiateItem(item);
                Log.d(TAG, "item added");
            } else {
                throw new AdjustmentDrawerException(ITEM_ALREADY_EXIST);
            }
        } else {
            throw new AdjustmentDrawerException(MAX_ITEMS_REACHED);
        }
    }

    private void instantiateItem(DrawerItem item){
        LinearLayout parent = (LinearLayout)LayoutInflater.from(context)
                .inflate(R.layout.camera_adjustment_item, this, false);

        ImageView btn = (ImageView)parent.findViewById(R.id.adjust_btn);
        TextView info = (TextView)parent.findViewById(R.id.adjust_text);

        btn.setId(item.getValue());

        switch (item){
            case EDIT:{
                btn.setImageResource(R.drawable.ic_edit_black_24dp);
                info.setText(R.string.ca_edit_title);
                break;
            }
            case EXPOSURE:{
                btn.setImageResource(R.drawable.ic_collections_black_24dp);
                info.setText(R.string.ca_exposure_title);
                parent.addView(instantiateSeekBar(item));
                break;
            }
            case OVERLAY_OPACITY:{
                btn.setImageResource(R.drawable.ic_opacity_black_24dp);
                info.setText(R.string.ca_opacity_title);
                parent.addView(instantiateSeekBar(item));
                break;
            }
            case WHITE_BALANCE:{
                btn.setImageResource(R.drawable.ic_filter_white_24dp);
                info.setText(R.string.ca_wb_title);
                parent.addView(instantiateSeekBar(item));
                break;
            }
        }

        parent.setAlpha(0);
        info.setX(-mItemSize);
        btn.setOnClickListener(this);
        container.addView(parent);
        updateStartPositions();

        Log.d(TAG, "Position: " + parent.getY());
    }

    private void updateStartPositions( ){
        int count = container.getChildCount();
        for(int i = 0; i < count; i ++){
            LinearLayout v = (LinearLayout)container.getChildAt(i);
            v.setY(count * (int) mItemSize);
            v.setGravity(Gravity.BOTTOM);
        }
        this.mStartPosition = count * (int)mItemSize;
        container.setGravity(Gravity.BOTTOM);
        container.getLayoutParams().height = (int)(count * mItemSize);
        rotateDrawer(0);
        container.invalidate();
        this.invalidate();
    }

    private SeekBar instantiateSeekBar(DrawerItem item){
        SeekBar seekBar = new SeekBar(context);
        seekBar.setId(item.getValue());
        seekBar.setMax(100);
        seekBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        seekBar.setOnSeekBarChangeListener(seekChangeListener);
        seekBar.setVisibility(GONE);
        seekBar.setAlpha(0f);
        return seekBar;
    }

    public interface AdjustmentDrawerListener{
        void onOpacityChanged(int value);
        void onExposureChanged(int value);
        void onWhiteBalanceChanged(int value);
    }

    private SeekBar.OnSeekBarChangeListener seekChangeListener
            = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            DrawerItem currItem = DrawerItem.values()[seekBar.getId()];
            switch (currItem){
                case OVERLAY_OPACITY:{
                    AdjustmentDrawer.this.mOpacity = progress;
                    if(listener != null) listener.onOpacityChanged(progress);
                    break;
                }
                case WHITE_BALANCE:{
                    AdjustmentDrawer.this.mWhiteBalance = progress;
                    if(listener != null) listener.onWhiteBalanceChanged(progress);
                    break;
                }
                case EXPOSURE:{
                    AdjustmentDrawer.this.mExposure = progress;
                    if(listener != null) listener.onExposureChanged(progress);
                    break;
                }
                default: Log.d(TAG, "Id unknown: " + seekBar.getId());
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public class AdjustmentDrawerException extends Exception{
        public AdjustmentDrawerException(String detailMessage) {
            super(detailMessage);
        }
    }

}
