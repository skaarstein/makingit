package com.skaarstein.makinit.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterViewFlipper;

import com.skaarstein.makinit.R;

/**
 * Created by Martin on 11.04.2016.
 */
public class GalleryFlipper extends AdapterViewFlipper{

    private static final String TAG = GalleryFlipper.class.getName();
    private Context context;

    public GalleryFlipper(Context context) {
        super(context);
        init(context);
    }

    public GalleryFlipper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GalleryFlipper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    public GalleryFlipper(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context){
        this.context = context;
    }

    private static final int MIN_DISTANCE = 120;
    private float downX, downY, upX, upY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP: {
                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                // swipe horizontal?
                if(Math.abs(deltaX) > Math.abs(deltaY))
                {
                    if(Math.abs(deltaX) > MIN_DISTANCE){
                        // left or right
                        if(deltaX > 0) {
                            showPrevious();
                            return true;
                        }
                        if(deltaX < 0) {
                            showNext();
                            return true;
                        }
                    }
                    else {
                        return false; // We don't consume the event
                    }
                }

                return true;
            }
        }
        return false;
    }
}
