package com.skaarstein.makinit.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.skaarstein.makinit.edit.ImageInfo;
import com.skaarstein.makinit.edit.ProjectInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Martin on 07.03.2016.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHandler.class.getName();

    public static final String PROJECT_TABLE = "project";
    public static final String IMAGE_INFO_TABLE = "image_info";
    public static final String IMAGE_DATA_TABLE = "image_data";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Makinit.db";

    public static final String PROJECT_ID = "_id";
    public static final String PROJECT_NAME = "project_name";
    public static final String PROJECT_DESCRIPTION = "project_desc";
    public static final String PROJECT_UPDATED = "project_updated";
    public static final String PROJECT_CREATED = "project_created";
    public static final String PROJECT_COVER = "project_cover";

    public static final String[] PROJECT_QUARRY = {PROJECT_ID, PROJECT_NAME, PROJECT_DESCRIPTION,
            PROJECT_UPDATED, PROJECT_CREATED, PROJECT_COVER};

    public static final String IMAGE_ID = "_id";
    public static final String IMAGE_PROJECT = "image_prj";
    public static final String IMAGE_NAME = "image_name";
    public static final String IMAGE_DESCRIPTION = "image_desc";
    public static final String IMAGE_CAPTURED = "image_captured";

    public static final String[] IMAGE_QUARRY = {IMAGE_ID, IMAGE_PROJECT, IMAGE_NAME,
            IMAGE_DESCRIPTION, IMAGE_CAPTURED};

    public static final String DATA_ID = "_id";
    public static final String DATA_PROJECT = "data_prj";
    public static final String DATA_IMAGE = "data_image";

    public static final String[] DATA_QUARRY = {DATA_ID, DATA_PROJECT, DATA_IMAGE};

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + PROJECT_TABLE + "("
                + PROJECT_ID + " INTEGER PRIMARY KEY," + PROJECT_NAME + " TEXT,"
                + PROJECT_DESCRIPTION + " TEXT," + PROJECT_UPDATED + " TEXT,"
                + PROJECT_CREATED + " TEXT," + PROJECT_COVER + " INTEGER)";
        db.execSQL(CREATE_TABLE);

        Log.d(TAG, "Created table: " + PROJECT_TABLE);

        CREATE_TABLE = "CREATE TABLE " + IMAGE_INFO_TABLE + "("
                + IMAGE_ID + " INTEGER PRIMARY KEY," + IMAGE_PROJECT + " INTEGER,"
                + IMAGE_NAME + " TEXT," + IMAGE_DESCRIPTION + " TEXT," + IMAGE_CAPTURED + " TEXT)";
        db.execSQL(CREATE_TABLE);

        Log.d(TAG, "Created table: " + IMAGE_INFO_TABLE);

        CREATE_TABLE = "CREATE TABLE " + IMAGE_DATA_TABLE + "("
                + DATA_ID + " INTEGER PRIMARY KEY," + DATA_PROJECT
                + " INTEGER," + DATA_IMAGE + " TEXT)";
        db.execSQL(CREATE_TABLE);

        Log.d(TAG, "Created table: " + IMAGE_DATA_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE IF EXISTS " + PROJECT_TABLE);
        Log.d(TAG, "Dropping table: " + PROJECT_TABLE);

        db.execSQL("DROP TABLE IF EXISTS " + IMAGE_INFO_TABLE);
        Log.d(TAG, "Dropping table: " + IMAGE_INFO_TABLE);

        db.execSQL("DROP TABLE IF EXISTS " + IMAGE_DATA_TABLE);
        Log.d(TAG, "Dropping table: " + IMAGE_DATA_TABLE);

        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
        Log.d(TAG, DATABASE_NAME + " downgraded");
    }
    public void updateProject(ProjectInfo prj){

        try {
            Log.d(TAG, "Updating project: " + prj.ID);
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            if(prj.name != null)
                values.put(PROJECT_NAME, prj.name);
            if(prj.description != null)
                values.put(PROJECT_DESCRIPTION, prj.description);
            if(prj.updated != null)
                values.put(PROJECT_UPDATED, prj.updated);
            if(prj.cover > 0)
                values.put(PROJECT_COVER, prj.cover);

            db.update(PROJECT_TABLE, values, PROJECT_ID + "=" + prj.ID, null);
            db.close();

            Log.d(TAG, "Project updated");

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }
    public void addProject(ProjectInfo prj) {
        try {
            Log.d(TAG, "Creating new project: " + prj.ID);
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(PROJECT_ID, prj.ID);
            if(prj.name != null)
                values.put(PROJECT_NAME, prj.name);
            if(prj.description != null)
                values.put(PROJECT_DESCRIPTION, prj.description);
            if(prj.updated != null)
                values.put(PROJECT_UPDATED, prj.updated);
            if(prj.created != null)
                values.put(PROJECT_CREATED, prj.created);
            if(prj.cover > 0)
                values.put(PROJECT_COVER, prj.cover);

            db.insert(PROJECT_TABLE, null, values);
            db.close();
            Log.d(TAG, "Project created, data size: " + values.size());

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    public void updateImage(ImageInfo image){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Log.d(TAG, "Updating image: " + image.ID);

            ContentValues values = new ContentValues();
            if (image.title != null)
                values.put(IMAGE_NAME, image.title);
            if (image.description != null)
                values.put(IMAGE_DESCRIPTION, image.description);

            db.update(IMAGE_INFO_TABLE, values, IMAGE_ID + "=" + image.ID, null);

            if(image.imageData != null) {
                values = new ContentValues();
                values.put(DATA_IMAGE, image.imageData);
                db.update(IMAGE_DATA_TABLE, values, DATA_ID + "=" + image.ID, null);
            }

            db.close();

            Log.d(TAG, "Image updated");
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    public void addImage(ImageInfo image) {
        try {
            Log.d(TAG, "Creating image: " + image.ID);
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(IMAGE_ID, image.ID);
            values.put(IMAGE_PROJECT, image.projectID);
            if(image.title != null)
                values.put(IMAGE_NAME, image.title);
            if(image.description != null)
                values.put(IMAGE_DESCRIPTION, image.description);
            if(image.date != null)
                values.put(IMAGE_CAPTURED, image.date);

            db.insert(IMAGE_INFO_TABLE, null, values);

            values = new ContentValues();
            values.put(DATA_ID, image.ID);
            values.put(DATA_PROJECT, image.projectID);
            if(image.imageData != null)
                values.put(DATA_IMAGE, image.imageData);

            db.insert(IMAGE_DATA_TABLE, null, values);
            db.close();
            Log.d(TAG, "Image created, data size: " + values.size());

            DatabaseListener.onNewImageAdded(image.projectID);

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    public Cursor getProjectCursor(){
        try {
            Log.d(TAG, "Retrieving project cursor");
            String query = "SELECT * FROM " + PROJECT_TABLE;
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            Log.d(TAG, "Cursor found");
            return cursor;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public ProjectInfo getProject(int projectID){
        try {
            String query = "SELECT * FROM " + PROJECT_TABLE + " WHERE " + PROJECT_ID + "=" + projectID;
            Log.d(TAG, "Retrieving project: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            ProjectInfo projectInfo = null;
            if (cursor.moveToFirst()) {
                Log.d(TAG, "Project found");
                projectInfo = new ProjectInfo(cursor.getInt(0), cursor.getString(4));
                Log.d(TAG, "Project id: " + projectInfo.ID + ", craated: " + projectInfo.created);
                projectInfo.name = cursor.getString(1);
                Log.d(TAG, "Name: " + projectInfo.name);
                projectInfo.description = cursor.getString(2);
                Log.d(TAG, "Description: " + projectInfo.description);
                projectInfo.updated = cursor.getString(3);
                Log.d(TAG, "Updated: " + projectInfo.updated);
                projectInfo.cover = cursor.getInt(5);
                Log.d(TAG, "Cover id: " + projectInfo.cover);
            }

            cursor.close();
            db.close();
            return projectInfo;

        } catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public ProjectInfo[] getProjects(){
        try {

            String query = "SELECT * FROM " + PROJECT_TABLE;
            Log.d(TAG, "Retrieving projects: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            ArrayList<ProjectInfo> projects = new ArrayList<>();

            while (cursor.moveToNext()) {
                ProjectInfo projectInfo = new ProjectInfo(cursor.getInt(0), cursor.getString(4));
                projectInfo.name = cursor.getString(1);
                projectInfo.description = cursor.getString(2);
                projectInfo.updated = cursor.getString(3);
                projectInfo.cover = cursor.getInt(5);

                projects.add(projectInfo);
            }

            cursor.close();
            db.close();

            if (projects.size() == 0) {
                Log.d(TAG, "No projects found");
                return null;
            }else {
                Log.d(TAG, "Found " + projects.size() + " projects");
                return projects.toArray(new ProjectInfo[projects.size()]);
            }
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public int deleteImage(int imageID){
        try {
            Log.d(TAG, "Deleting image: " + imageID);
            SQLiteDatabase db = getWritableDatabase();
            int deleted = db.delete(IMAGE_INFO_TABLE, IMAGE_ID + "=" + imageID, null);
            if(deleted > 0)
                deleted = db.delete(IMAGE_INFO_TABLE, DATA_ID + "=" + imageID, null);
            Log.d(TAG, deleted + " images deleted");
            return deleted;
        } catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return 0;
        }
    }

    public int deleteProject(int projectID){
        try {
            Log.d(TAG, "Deleting project: " + projectID);
            SQLiteDatabase db = getWritableDatabase();
            int deleted = db.delete(PROJECT_TABLE, PROJECT_ID + "=" + projectID, null);
            Log.d(TAG, deleted + " projects deleted");
            deleted += db.delete(IMAGE_INFO_TABLE, IMAGE_PROJECT + "=" + projectID, null);
            deleted += db.delete(IMAGE_DATA_TABLE, DATA_PROJECT + "=" + projectID, null);
            Log.d(TAG, deleted-1 + " entries deleted");
            return deleted;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return 0;
        }
    }

    public Cursor getImageInfoCursor(int projectID){
        try {
            Log.d(TAG, "retrieving image info cursor for project: " + projectID);
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_PROJECT + "=" + projectID;
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            Log.d(TAG, "Cursor found");
            return cursor;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public Cursor getImageDataCursor(int projectID){
        try {
            Log.d(TAG, "retrieving image data cursor for project: " + projectID);
            String query = "SELECT * FROM " + IMAGE_DATA_TABLE + " WHERE " + DATA_PROJECT + "=" + projectID;
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            Log.d(TAG, "Cursor found");
            return cursor;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public ImageInfo getImageInfo(int imageID){
        try {
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_ID + "=" + imageID;
            Log.d(TAG, "Retrieving image info: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            ImageInfo image = null;
            if (cursor.moveToFirst()) {
                image = new ImageInfo(cursor.getInt(0), cursor.getInt(1),
                        cursor.getString(4));

                image.title = cursor.getString(2);
                image.description = cursor.getString(3);

                Log.d(TAG, "Image info found");
            }
            cursor.close();
            db.close();
            return image;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public byte[] getImageData(int imageID){
        try {
            String query = "SELECT * FROM " + IMAGE_DATA_TABLE + " WHERE " + DATA_ID + "=" + imageID;
            Log.d(TAG, "Retrieving image data: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            byte[] data = null;

            if (cursor.moveToFirst()) {
                data = cursor.getBlob(2);
                Log.d(TAG, "Image data found, data size: " + data.length);
            }

            cursor.close();
            db.close();

            return data;
        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public ImageInfo getImageInfoWithData(int imageID){
        ImageInfo img = getImageInfo(imageID);
        img.imageData = getImageData(imageID);
        return img;
    }

    public ImageInfo getImageOverlay(int projectID) {
        try {
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_PROJECT + "=" + projectID;
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            ImageInfo last = null;
            while (cursor.moveToNext()) {
                ImageInfo image = new ImageInfo(cursor.getInt(0), cursor.getInt(1),
                        cursor.getString(4));

                SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
                Date current = sdf.parse(image.date);

                image.title = cursor.getString(2);
                image.description = cursor.getString(3);
                image.imageData = cursor.getBlob(5);

                if(last == null) last = image;
                else if(current.after(sdf.parse(last.date)))
                    last = image;

            }

            cursor.close();
            db.close();
            return last;

        }catch (ParseException ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }
    public int getImageCount(int projectID){
        try {
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_PROJECT + "=" + projectID;
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            int count = 0;
            while (cursor.moveToNext()) {
                count += 1;
            }

            cursor.close();
            db.close();
            return count;

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return 0;
        }
    }
    public interface ImageInfoLoader{
        void onImageLoaded(ImageInfo imageInfo, int position);
        void onError(int position, String message);
    }

    public void loadImages(int projectID, ImageInfoLoader loader){
        try {
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_PROJECT + "=" + projectID;
            Log.d(TAG, "Loading images from project: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            int position = 0;
            while (cursor.moveToNext()) {

                ImageInfo info = new ImageInfo(cursor.getInt(0), cursor.getInt(1),
                        cursor.getString(4));

                info.title = cursor.getString(2);
                info.description = cursor.getString(3);
                info.imageData = getImageData(info.ID);

                if(info != null) {
                    Log.d(TAG, "Image loaded");
                    loader.onImageLoaded(info, position);
                }
                else loader.onError(position, "missing image");
                position += 1;
            }

            cursor.close();
            db.close();

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }
    public Integer[] getImageIDs(int projectID){
        try {
            String query = "SELECT * FROM " + IMAGE_INFO_TABLE + " WHERE " + IMAGE_PROJECT + "=" + projectID;
            Log.d(TAG, "Retrieving image IDs for project: " + query);

            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            ArrayList<Integer> images = new ArrayList<>();

            while (cursor.moveToNext()) {
                images.add(cursor.getInt(0));
            }

            cursor.close();
            db.close();

            if (images.size() == 0) {
                Log.d(TAG, "No images found for project: " + projectID);
                return null;
            } else {
                Log.d(TAG, images.size() + " images found for project: " + projectID);
                return images.toArray(new Integer[images.size()]);
            }

        }catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }
}
