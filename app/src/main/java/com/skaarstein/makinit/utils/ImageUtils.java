package com.skaarstein.makinit.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

/**
 * Created by Martin on 09.03.2016.
 */
public class ImageUtils {

    public static Bitmap scaleImage(byte[] data, ImageResolution resolution){
        try {

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;

            Bitmap loadedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, bmpFactoryOptions);

            int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) resolution.imgHeight);
            int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) resolution.imgWidth);

            if(heightRatio > 1 || widthRatio > 1)
            {
                if(heightRatio > widthRatio)
                {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                }
                else
                {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            loadedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, bmpFactoryOptions);
            return loadedBitmap;

        }  catch (Exception e) {
            Log.e("Error reading file", e.toString());
            return null;
        }
    }

    public static byte[] compressBitmap(Bitmap bitmap){
        return compressBitmap(bitmap, 100, Bitmap.CompressFormat.PNG);
    }

    public static byte[] compressBitmap(Bitmap bitmap, int amount, Bitmap.CompressFormat format){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(format, amount, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap rotateBitmap(byte[] data){
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        return SlyExifUtil.rotateBitmap(data, bitmap);
    }
}
