package com.skaarstein.makinit.utils;

import android.animation.TimeInterpolator;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

/**
 * Created by Martin on 17.04.2016.
 */
public class AdjustmentDrawerAnimator {
    
    private int mTextDuration;
    private int mDisplayDuration;
    private int mTextDelay;
    private int mDisplayDelay;
    private int mMaxItems;
    private TimeInterpolator mDisplayInterpolator;
    private TimeInterpolator mTextInterpolator;
    private int mIconWidth;
    private int mIconHeight;
    private int mItemHeight;
    private int mItemWidth;
    private int mDrawerMargin;
    private RectF mContainer;
    private PointF mAnchor;
    private SparseArray<DrawerItemState> mItemStates;

    private boolean mDrawerOpen;
    private boolean mItemSelected;
    private int mSelectedIndex;
    private PointF mCenterPosition;
    private PointF mListAnchor;
    private DrawerDirection mCurrDirection;


    public enum DrawerItemState{
        SELECTED(0),
        VISIBLE(1),
        INVISIBLE(2),
        MOVING(4),
        NONE(5);

        private int value;
        DrawerItemState(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum DrawerItemSelectionType{
        CENTER(1),
        FOCUS(2),
        VOBBLE(3);

        private int value;
        DrawerItemSelectionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    
    public static final int DEFAULT_TEXT_DURATION = 200;
    public static final int DEFAULT_DISPLAY_DURATION = 400;
    public static final int DEFAULT_TEXT_DELAY = 0;
    public static final int DEFAULT_DISPLAY_DELAY = 70;
    
    public AdjustmentDrawerAnimator(int drawerMargin, int mIconWidth, int mIconHeight, int mItemHeight,
                                    int mItemWidth, RectF mContainer, PointF mAnchor, int maxItems) {
        this.mDrawerMargin = drawerMargin;
        this.mIconWidth = mIconWidth;
        this.mIconHeight = mIconHeight;
        this.mItemHeight = mItemHeight;
        this.mItemWidth = mItemWidth;
        this.mContainer = mContainer;
        this.mAnchor = mAnchor;
        this.mMaxItems = maxItems;
        this.mItemStates = new SparseArray();
        restoreDefaultValues();
        updateListAnchor(0);
    }
    
    public TimeInterpolator getDefaultDisplayInterpolator(){
        return new AccelerateDecelerateInterpolator();
    }
    
    public TimeInterpolator getDefaultTextInterpolator(){
        return new AccelerateDecelerateInterpolator();
    }

    public void setMaxItems(int maxItems){
        this.mMaxItems = maxItems;
    }
    
    public void restoreDefaultValues(){
        this.mTextDelay = DEFAULT_TEXT_DELAY;
        this.mTextDuration = DEFAULT_TEXT_DURATION;
        this.mTextInterpolator = getDefaultTextInterpolator();
        this.mDisplayDelay = DEFAULT_DISPLAY_DELAY;
        this.mDisplayDuration = DEFAULT_DISPLAY_DURATION;
        this.mDisplayInterpolator = getDefaultDisplayInterpolator();
        this.mCenterPosition = new PointF(mContainer.centerX() - (mItemWidth/2),
                mAnchor.y - mDrawerMargin * 3);
    }

    public void setTextPorpteries(int TextDuration, int TextDelay,
                                     TimeInterpolator TextInterpolator){
        this.mTextDuration = TextDuration;
        this.mTextInterpolator = TextInterpolator;
        this.mTextDelay = TextDelay;

    }

    public void setDisplayPorpteries(int DisplayDuration, int DisplayDelay,
                                  TimeInterpolator DisplayInterpolator){
        this.mDisplayDuration = DisplayDuration;
        this.mDisplayInterpolator = DisplayInterpolator;
        this.mDisplayDelay = DisplayDelay;

    }

    public void rotateDrawer(int rotation, ViewGroup container){
        int count = container.getChildCount();
        updateListAnchor(rotation);
        for(int i = 0; i < count; i++){
            rotateItem((ViewGroup)container.getChildAt(i), i, rotation);
        }
    }

    public void selectDeselectItem(int position, ViewGroup container,
                                   DrawerItemSelectionType selectionType, boolean hideDeselectedItems){
        int count = container.getChildCount();
        if(mItemSelected) {
            for (int i = 0; i < count; i++) {
                if(i == mSelectedIndex) {
                    deselectItem(i, container, hideDeselectedItems);
                } else {
                    selectItem(i, container, selectionType);
                }
            }
        } else {
            for (int i = 0; i < count; i++) {
                if (i == position){
                    selectItem(i, container, selectionType);
                } else {
                    deselectItem(i, container, hideDeselectedItems);
                }
            }
        }
    }

    public void resetStates(ViewGroup container){
        int count = container.getChildCount();
        for(int i = 0; i < count ; i ++){
            mItemStates.put(i, DrawerItemState.NONE);
        }
    }

    public void selectItem(int position, ViewGroup container, DrawerItemSelectionType selectionType){
        DrawerItemState state = mItemStates.get(position, DrawerItemState.NONE);
        if(state == DrawerItemState.VISIBLE) {
            switch (selectionType) {
                case CENTER:
                    selectItemC((ViewGroup) container.getChildAt(position), position);
                    break;
                case FOCUS:
                    selectItemF((ViewGroup) container.getChildAt(position), position);
                    break;
                case VOBBLE:
                    selectItemV((ViewGroup) container.getChildAt(position), position);
                    break;
            }
        } else {
            showItem(position, (ViewGroup) container.getChildAt(position));
        }
    }

    public void hideItem(final int position, final ViewGroup item){
        int delay = mDisplayDelay * position;
        mItemStates.put(position, DrawerItemState.MOVING);
        item.animate()
                .setDuration(mDisplayDuration + delay)
                .setInterpolator(mDisplayInterpolator)
                .setStartDelay(delay)
                .x(mAnchor.x)
                .y(mAnchor.y)
                .alpha(0f)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mItemStates.put(position, DrawerItemState.INVISIBLE);
                        item.setVisibility(View.INVISIBLE);
                    }
                }).start();
    }

    public void showItem(final int position, final ViewGroup item){
        int delay = mDisplayDelay * position;
        mItemStates.put(position, DrawerItemState.MOVING);
        PointF pos = getItemPosition(position, item);
        item.animate()
                .setDuration(mDisplayDuration + delay)
                .setInterpolator(mDisplayInterpolator)
                .setStartDelay(delay)
                .x(pos.x)
                .y(pos.y)
                .alpha(1f)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mItemStates.put(position, DrawerItemState.VISIBLE);
                        item.setVisibility(View.VISIBLE);
                    }
                }).start();
    }

    public void deselectItem(int position, ViewGroup container, boolean hideDeselectedItem){
        DrawerItemState state = mItemStates.get(position, DrawerItemState.NONE);
        switch (state){
            case NONE:
                mItemStates.append(position, DrawerItemState.VISIBLE);
            case VISIBLE:
                if(hideDeselectedItem) hideItem(position, (ViewGroup) container.getChildAt(position));
                break;
            case SELECTED:
                deselectItem((ViewGroup)container.getChildAt(position), position, hideDeselectedItem);
                break;
        }
    }

    public void deselectItem(ViewGroup container){
        deselectItem(mSelectedIndex, container, true);
    }

    public void showDrawer(ViewGroup container){

    }
    public void hideDrawer(ViewGroup container){

    }
    public void hideShowDrawer(ViewGroup container){

    }
    public void centerItem(ViewGroup container){

    }

    /**
     * Privates
     */
    private void rotateItem(final ViewGroup item, final int position, final int rotation){
        if(mItemStates.get(position, DrawerItemState.NONE) == DrawerItemState.VISIBLE){
            mItemStates.put(position, DrawerItemState.MOVING);
            final int delay = position * mTextDelay;
            item.animate()
                    .setInterpolator(mTextInterpolator)
                    .setDuration(mTextDuration + delay)
                    .setStartDelay(delay)
                    .alpha(0f)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            PointF pos = getItemPosition(position, item);
                            item.setRotation(rotation);
                            item.setY(pos.y);
                            item.setX(pos.x);
                            if(mItemStates.get(position, DrawerItemState.NONE)
                                    == DrawerItemState.MOVING) {
                                item.animate()
                                        .setInterpolator(mTextInterpolator)
                                        .setDuration(mTextDuration + delay)
                                        .setStartDelay(delay)
                                        .alpha(1f)
                                        .withEndAction(new Runnable() {
                                            @Override
                                            public void run() {
                                                mItemStates.put(position, DrawerItemState.VISIBLE);
                                            }
                                        }).start();
                            }
                        }
                    })
                    .start();
        } else {
            PointF pos = getItemPosition(position, item);
            item.setRotation(rotation);
            item.setY(pos.y);
            item.setX(pos.x);
        }
    }
    private void selectItemF(ViewGroup item, int position){

    }
    private void selectItemC(ViewGroup item, int position){
        View c = item.getChildAt(2);
        View b = item.getChildAt(0);
        View t = item.getChildAt(1);
        int delay = mTextDelay * position;
        mItemStates.put(position, DrawerItemState.MOVING);
        t.animate()
                .setInterpolator(mTextInterpolator)
                .setDuration(mTextDuration + delay)
                .setStartDelay(delay)
                .x(b.getX() + mIconWidth)
                .alpha(1f)
                .start();
        c.animate()
                .setInterpolator(mTextInterpolator)
                .setDuration(mTextDuration + delay)
                .setStartDelay(delay)
                .x(b.getX() - mIconWidth / 2)
                .alpha(0f)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        if(mItemStates.get(position, DrawerItemState.NONE)
                                == DrawerItemState.MOVING){
                            mItemStates.put(position, DrawerItemState.VISIBLE);
                            if (hideItem) {
                                hideItem(position, item);
                            }
                        }
                    }
                }).start();

        if(!hideItem){

        }
    }
    private void selectItemV(ViewGroup item, int position){

    }

    private void deselectItem(final ViewGroup item, final int position, final boolean hideItem){
        View c = item.getChildAt(2);
        View b = item.getChildAt(0);
        int delay = mTextDelay * position;
        mItemStates.put(position, DrawerItemState.MOVING);
        c.animate()
                .setInterpolator(mTextInterpolator)
                .setDuration(mTextDuration + delay)
                .setStartDelay(delay)
                .x(b.getX() - mIconWidth / 2)
                .alpha(0f)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        if(mItemStates.get(position, DrawerItemState.NONE)
                                == DrawerItemState.MOVING){
                            mItemStates.put(position, DrawerItemState.VISIBLE);
                            if (hideItem) {
                                hideItem(position, item);
                            }
                        }
                    }
                }).start();

        if(!hideItem){
            View t = item.getChildAt(1);
            t.animate()
                    .setInterpolator(mTextInterpolator)
                    .setDuration(mTextDuration + delay)
                    .setStartDelay(delay)
                    .x(b.getX() + mIconWidth)
                    .alpha(1f)
                    .start();
        }

    }
    private enum DrawerDirection{
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    private PointF getItemPosition(int position, ViewGroup item){
        switch (mCurrDirection){
            case UP:
                item.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                return new PointF(mListAnchor.x, mListAnchor.y - (mItemHeight * position));
            case DOWN:
                item.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                return new PointF(mListAnchor.x, mListAnchor.y + (mItemHeight * position));
            case LEFT:
                item.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                return new PointF(mListAnchor.x - (mItemHeight * position), mListAnchor.y);
            case RIGHT:
                item.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                return new PointF(mListAnchor.x + (mItemHeight * position), mListAnchor.y);
        }
        return null;
    }

    private void updateListAnchor(final int degrees) {
        switch (degrees){
            case 0:{
                mListAnchor = new PointF(mAnchor.x, mAnchor.y - mDrawerMargin);
                mCurrDirection = DrawerDirection.UP;
                break;
            }
            case 90:{
                mListAnchor = new PointF(mAnchor.x, mAnchor.y - mDrawerMargin);
                mCurrDirection = DrawerDirection.RIGHT;
                break;
            }
            case 180:{
                mListAnchor = new PointF(mAnchor.x, mAnchor.y - mDrawerMargin -
                        (mMaxItems * mItemHeight));
                mCurrDirection = DrawerDirection.DOWN;
                break;
            }
            case 270:{
                mListAnchor = new PointF(mAnchor.x + (mMaxItems * mItemHeight),
                        mAnchor.y - mDrawerMargin - mItemWidth);
                mCurrDirection = DrawerDirection.LEFT;
                break;
            }
        }
    }

}
