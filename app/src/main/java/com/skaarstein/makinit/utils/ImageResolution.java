package com.skaarstein.makinit.utils;

/**
 * Created by Martin on 24.02.2016.
 */
public class ImageResolution {

    public final double imgWidth;
    public final double imgHeight;

    public final static ImageResolution P480p = new ImageResolution(480, 853.33);
    public final static ImageResolution P360p = new ImageResolution(360, 730);
    public final static ImageResolution L1080p = new ImageResolution(1920, 1080);
    public final static ImageResolution P1080p = new ImageResolution(1080, 1920);
    public final static ImageResolution P768p = new ImageResolution(1280, 768);
    public final static ImageResolution P600p = new ImageResolution(600, 800);

    public ImageResolution(double imgWidth, double imgHeight) {
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
    }
}
