package com.skaarstein.makinit.utils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.nio.charset.Charset;
/**
 * Created by SlyOtis on 01.02.2016.
 */
public abstract class Serializer {
    /**
     * Convert data to JSON format and return it as an array of bytes.
     * @param data object to convert
     * @return data as an array of bytes
     */
    public static byte[] fromObject(Object data) {
        Gson parser = new Gson();
        String json = parser.toJson(data, data.getClass()).trim();
        Log.d("Serializer", json);
        // Converts JSON string to UTF-8 array of bytes
        return json.getBytes(Charset.defaultCharset());
    }

    /**
     * Convert data to object of type.
     *
     * @param data
     *            object as an array of bytes
     * @param type
     *            type of return object
     * @return object of type
     */
    public static <T> T toObject(byte[] data, Class<T> type) throws JsonSyntaxException, JsonParseException {
        // Converts byte array to UTF-8 JSON string
        String json = new String(data, Charset.defaultCharset()).trim();
        Log.d("Serializer", json);
        try {
            Gson parser = new Gson();
            return parser.fromJson(json, type);
        } catch (JsonSyntaxException e1) {
            Log.e(Serializer.class.getName(), json + " is a malformed JSON");
            throw e1;
        } catch (JsonParseException e2) {
            throw e2;
        }
    }
}
