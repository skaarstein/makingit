package com.skaarstein.makinit.utils;

import java.util.ArrayList;

/**
 * Created by Martin on 12.04.2016.
 */
public class DatabaseListener {
    private static ArrayList<DataUpdateListener> listeners;

    public interface DataUpdateListener{
        void onNewImageAdded(int projectID);
    }

    public static void addDataUpdateListener(DataUpdateListener updateListener){
        if(listeners == null) listeners = new ArrayList<>();
        listeners.add(updateListener);
    }

    public static void onNewImageAdded(int projectID){
        for(DataUpdateListener l: listeners){
            l.onNewImageAdded(projectID);
        }
    }
}
