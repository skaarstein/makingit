package com.skaarstein.makinit.slycamera;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.skaarstein.slycamera.SlyCameraControls;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Martin on 03.04.2016.
 */
public class SlyCameraPagerAdapter extends FragmentStatePagerAdapter {

    private SlyCameraControls controls;
    private PagerControls pager;
    private FragmentManager fm;
    private int ID;

    public SlyCameraPagerAdapter(FragmentManager fm, int ID, PagerControls pager,
                                 SlyCameraControls controls) {
        super(fm);
        this.pager = pager;
        this.controls = controls;
        this.ID = ID;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                OverlayFragment overlayFragment = OverlayFragment.newInstance(ID, pager);
                return overlayFragment;
            case 1:
                CameraFragment cameraFragment = CameraFragment.newInstance(ID, pager, controls);
                return cameraFragment;
            case 2:
                SettingsFragment settingsFragment =
                        SettingsFragment.newInstance(ID, pager, controls);
                return settingsFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public interface PagerControls{
        void setCurrentItem(int index, boolean smooth);
        void close();
    }
}
