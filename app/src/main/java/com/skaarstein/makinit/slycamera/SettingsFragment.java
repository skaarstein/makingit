package com.skaarstein.makinit.slycamera;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.skaarstein.makinit.slycamera.SlyCameraPagerAdapter.*;

import com.skaarstein.makinit.R;
import com.skaarstein.slycamera.SlyCameraControls;

/**
 * Created by Martin on 03.04.2016.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener{

    private static final String TAG = SettingsFragment.class.getName();

    public static final String PREF_NAME_SETTINGS = "camera_settings";
    public static final String PREF_CLEAR_CACHE_ON_CLOSE = "clear_on_close";
    public static final String PREF_SAVE_TO_GALLERY = "save_to_gallery";
    public static final String PREF_PROMPT_TO_SAVE = "clear_on_close";
    public static final String PREF_COVER_OPACITY = "cover_opacity";
    public static final String PREF_COVER_ID = "cover_id";

    private Switch autoClearCache;
    private Switch saveToGallery;
    private Switch autoSaveSettings;
    private Switch promptToSave;
    private TextView cacheSizeValue;
    private TextView clearCatchValue;
    private SeekBar cacheSize;

    private PagerControls pagerControls;
    private SlyCameraControls controls;
    private int ID;

    public static SettingsFragment newInstance(int ID, PagerControls pagerControls, SlyCameraControls controls){
        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.ID = ID;
        settingsFragment.pagerControls = pagerControls;
        settingsFragment.controls = controls;
        return settingsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null){
            view.findViewById(R.id.cs_open_camera).setOnClickListener(this);
            view.findViewById(R.id.cs_close_camera).setOnClickListener(this);
            view.findViewById(R.id.cs_clear_cache).setOnClickListener(this);

            autoClearCache = (Switch)view.findViewById(R.id.cs_auto_clear_cache);
            saveToGallery = (Switch)view.findViewById(R.id.cs_save_to_gallery);
            autoSaveSettings = (Switch)view.findViewById(R.id.cs_auto_save_settings);
            promptToSave = (Switch)view.findViewById(R.id.cs_prompt_to_save);
            cacheSize = (SeekBar)view.findViewById(R.id.cs_catch_size);
            cacheSizeValue = (TextView)view.findViewById(R.id.cs_catch_size_value);

            loadSettings();
        }
    }


    private void loadSettings(){
        if(autoClearCache != null){
            SharedPreferences pref = getActivity().getSharedPreferences(PREF_NAME_SETTINGS,
                    Context.MODE_PRIVATE);

            autoClearCache.setChecked(pref.getBoolean(PREF_CLEAR_CACHE_ON_CLOSE, true));
            saveToGallery.setChecked(pref.getBoolean(PREF_SAVE_TO_GALLERY, false));
            autoSaveSettings.setChecked(controls.getSaveSettingsOnClose());
            promptToSave.setChecked(pref.getBoolean(PREF_PROMPT_TO_SAVE, true));
            clearCatchValue.setText(controls.getUsedCacheSize()/1000 + " MB");
            int size = controls.getCameraCatchSize()/1000;
            cacheSizeValue.setText(size + " MB");
            cacheSize.setProgress(size);
        }
    }

    private void saveSetting(){
        if(autoClearCache != null) {
            SharedPreferences.Editor editor = getActivity().getSharedPreferences(PREF_NAME_SETTINGS,
                    Context.MODE_PRIVATE).edit();

            editor.putBoolean(PREF_CLEAR_CACHE_ON_CLOSE, autoClearCache.isChecked());
            editor.putBoolean(PREF_SAVE_TO_GALLERY, saveToGallery.isChecked());
            editor.putBoolean(PREF_PROMPT_TO_SAVE, promptToSave.isChecked());
            editor.commit();

            controls.setCameraCatchSize(cacheSize.getProgress());
            controls.setSaveSettingsOnClose(autoSaveSettings.isChecked());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadSettings();
        Log.d(TAG, "Tha real resume");
    }

    @Override
    public void onPause() {
        super.onPause();
        saveSetting();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cs_open_camera:{
                pagerControls.setCurrentItem(1, true);
                break;
            }
            case R.id.cs_close_camera:{
                pagerControls.close();
                break;
            }
            case R.id.cs_clear_cache:{
                controls.clearCameraCatch(true);
                clearCatchValue.setText("0 MB");
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        cacheSizeValue.setText(progress + " MB");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
