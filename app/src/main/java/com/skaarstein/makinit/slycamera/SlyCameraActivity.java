package com.skaarstein.makinit.slycamera;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.skaarstein.makinit.Home;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.views.VerticalViewPager;
import com.skaarstein.slycamera.SlyCamera;
import com.skaarstein.slycamera.SlyCameraListener;
import com.skaarstein.makinit.slycamera.SlyCameraPagerAdapter.*;

import java.io.File;

/**
 * Created by Martin on 03.04.2016.
 */
public class SlyCameraActivity extends AppCompatActivity implements SlyCameraListener,
        SlyCameraPagerAdapter.PagerControls{

    private static final String TAG = SlyCameraActivity.class.getName();
    private SlyCamera slyCamera;
    private SlyCameraPagerAdapter adapter;
    private VerticalViewPager pager;

    private int ID;

    private class VerticalPageTransformer implements ViewPager.PageTransformer {

        private View background;

        public VerticalPageTransformer(@NonNull View background) {
            this.background = background;
        }

        @Override
        public void transformPage(View view, float position) {
            if (position < -1) {
                view.setAlpha(0);
            } else if (position <= 1) {

                if (position < 0) {
                    view.setAlpha(1 + position);
                    background.setAlpha(1 + position - 0.45f);
                } else {
                    view.setAlpha(1 - position);
                    background.setAlpha(1 - position - 0.45f);
                }

                view.setTranslationX(view.getWidth() * -position);

                float yPosition = position * view.getHeight();
                view.setTranslationY(yPosition);
            } else {
                view.setAlpha(0);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            setContentView(R.layout.activity_sly_camera);

            ID = getIntent().getExtras().getInt(Home.ID_KEY, -1);
            if(ID == -1) return;

            slyCamera = (SlyCamera) findViewById(R.id.slyCamera);
            slyCamera.startCamera(this);
            slyCamera.addSlyCameraListener(this);


            pager = (VerticalViewPager) findViewById(R.id.slyCamera_pager);
            adapter = new SlyCameraPagerAdapter(getFragmentManager(), ID, this, slyCamera);
            pager.setAdapter(adapter);
            adapter.setPrimaryItem(pager, 1, adapter.getItem(1));
            pager.setCurrentItem(1);
            pager.setOffscreenPageLimit(0);
            pager.setPageTransformer(true,
                    new VerticalPageTransformer(findViewById(R.id.slyCamera_pager_bck)));
        }
    }

    @Override
    public void setCurrentItem(int index, boolean smooth) {
        pager.setCurrentItem(index, smooth);
    }

    @Override
    public void close() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(slyCamera != null)
            slyCamera.startCamera(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(slyCamera != null)
            slyCamera.stopCamera();
    }

    @Override
    public void onBackPressed() {

        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            return;
        }

        if (pager.getCurrentItem() == 1) {

            SharedPreferences pref = getSharedPreferences(
                    SettingsFragment.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);

            if(pref.getBoolean(SettingsFragment.PREF_CLEAR_CACHE_ON_CLOSE, true))
                slyCamera.clearCameraCatch(false);

            super.onBackPressed();
        } else {
            pager.setCurrentItem(1);
            return;
        }
    }

    @Override
    public void onCameraRotated(int orientation) {

    }

    @Override
    public void onCameraCacheCleared(long sizeCleared, int nrOfFilesRemoved, boolean forcedClear) {

    }

    @Override
    public void onUnlockFocus() {

    }

    @Override
    public void onLockFocus() {

    }

    @Override
    public void onPreCapture() {

    }

    @Override
    public void onCaptureComplete(File imageFile) {

    }

    @Override
    public void onError(int code, String msg) {

    }
}
