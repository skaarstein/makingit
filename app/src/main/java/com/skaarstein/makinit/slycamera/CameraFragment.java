package com.skaarstein.makinit.slycamera;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.edit.ProjectInfo;
import com.skaarstein.makinit.services.ImageSaveIntent;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.slycamera.SlyCameraListener;
import com.skaarstein.slycamera.SlyCamera;
import com.skaarstein.slycamera.SlyCameraControls;
import com.skaarstein.makinit.slycamera.SlyCameraPagerAdapter.*;

import java.io.File;

/**
 * Created by Martin on 03.04.2016.
 */
public class CameraFragment extends Fragment implements View.OnClickListener, SlyCameraListener {

    private static final String TAG = CameraFragment.class.getName();
    private SlyCameraControls controls;
    private PagerControls pager;
    private int ID;
    private int coverID;
    private int coverOpacity;

    private ImageView overlay;
    private ImageButton overlayToggle;
    private ImageButton flashToggle;
    private ImageButton switchToggle;
    private ImageButton settings;
    private ImageButton adjustments;
    private ImageButton close;
    private ImageButton capture;

    private boolean mOverlayVisible;
    private boolean mInitialized;
    private boolean mSaveToGallery;
    private boolean mPromptToSave;

    public static CameraFragment newInstance(int ID, PagerControls pager, SlyCameraControls controls){
        CameraFragment cameraFragment = new CameraFragment();
        cameraFragment.controls = controls;
        cameraFragment.pager = pager;
        cameraFragment.ID = ID;
        controls.addSlyCameraListener(cameraFragment);
        return cameraFragment;
    }

    private class OverlayLoader extends AsyncTask<Integer, Integer, Bitmap> {

        private final Context context;

        public OverlayLoader(Context context) {
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            try{

                DatabaseHandler dh = new DatabaseHandler(context);

                if(coverID < 0){
                    ProjectInfo inf = dh.getProject(params[0]);
                    coverID = inf.cover;
                }

                Log.d(TAG, "coverID: " + coverID);
                byte[] data = dh.getImageData(coverID);
                Bitmap tmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                if(tmp == null) Log.d(TAG, "doInBackground: yeah no image");
                return tmp;

            }catch (Exception ex){
                Log.e(TAG, ex.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if(bitmap != null){
                overlay.setImageBitmap(bitmap);
                overlay.setAlpha(coverOpacity / 100f);
                mOverlayVisible = true;
                updateOverlay();
                Log.d(TAG, "Overlay image set");
            } else {
                mOverlayVisible = false;
                updateOverlay();
            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sly_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null){
            settings =(ImageButton)view.findViewById(R.id.camera_settings);
            settings.setOnClickListener(this);
            adjustments = (ImageButton)view.findViewById(R.id.camera_adjustments);
            adjustments.setOnClickListener(this);
            close = (ImageButton)view.findViewById(R.id.camera_close);
            close.setOnClickListener(this);
            capture = (ImageButton)view.findViewById(R.id.camera_picture);
            capture.setOnClickListener(this);
            overlay = (ImageView)view.findViewById(R.id.screen_overlay);
            overlayToggle = (ImageButton)view.findViewById(R.id.camera_overlay);
            overlayToggle.setOnClickListener(this);
            flashToggle = (ImageButton)view.findViewById(R.id.camera_flash);
            flashToggle.setOnClickListener(this);
            switchToggle = (ImageButton)view.findViewById(R.id.camera_switch);
            switchToggle.setOnClickListener(this);
            coverID = -1;
            mInitialized = true;
        }
    }

    private void rotateImage(int orientation){
        switch (orientation){
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:{

                break;
            }
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:{

                break;
            }
        }
    }

    private int lastDegree = 0;
    int mRotationDuration = 300;
    private void rotateControls(int orientation){


        int currDegree = 0;
        RotateAnimation anim;

        switch (orientation){
            case Surface.ROTATION_0:{
                if(lastDegree == -270) lastDegree = 90;
                currDegree = 0;
                break;
            }
            case Surface.ROTATION_90:{
                if(lastDegree == -180) currDegree = -270;
                else currDegree = 90;
                break;
            }
            case Surface.ROTATION_180:{
                if(lastDegree == -90) currDegree = -180;
                else currDegree = 180;
                break;
            }
            case Surface.ROTATION_270:{
                if(lastDegree == 180) currDegree = 270;
                else currDegree = -90;
                break;
            }
        }

        if(currDegree != lastDegree) {
            anim = new RotateAnimation(lastDegree, currDegree,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(mRotationDuration);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);

            overlayToggle.startAnimation(anim);
            flashToggle.startAnimation(anim);
            switchToggle.startAnimation(anim);
            //settings.startAnimation(anim);
            //overlaySettings.startAnimation(anim);
            adjustments.startAnimation(anim);
            close.startAnimation(anim);

            anim = new RotateAnimation(lastDegree, currDegree,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(mRotationDuration);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);

            capture.startAnimation(anim);

            lastDegree = currDegree;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadSettings();
        OverlayLoader overlayLoader = new OverlayLoader(getActivity());
        overlayLoader.execute(ID);
        Log.d(TAG, "Camera resumed");
    }

    private void loadSettings(){
        Activity activity = getActivity();
        if(activity != null) {
            SharedPreferences pref = getActivity().getSharedPreferences(
                    SettingsFragment.PREF_NAME_SETTINGS, Context.MODE_PRIVATE);

            mPromptToSave = pref.getBoolean(SettingsFragment.PREF_PROMPT_TO_SAVE, true);
            mSaveToGallery = pref.getBoolean(SettingsFragment.PREF_SAVE_TO_GALLERY, false);
            coverID = pref.getInt(SettingsFragment.PREF_COVER_ID, -1);
            coverOpacity = pref.getInt(SettingsFragment.PREF_COVER_OPACITY, 75);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.camera_settings:{
                pager.setCurrentItem(2, true);
                break;
            }
            case R.id.camera_overlay:{
                if(mOverlayVisible) mOverlayVisible = false;
                else mOverlayVisible = true;
                updateOverlay();
                break;
            }
            case R.id.camera_close:{
                pager.close();
                break;
            }
            case R.id.camera_adjustments:{
                cameraAdjustments();
                break;
            }
            case R.id.camera_picture:{
                controls.takePicture();
                break;
            }
            case R.id.camera_flash:{
                updateFlash();
                break;
            }
            case R.id.camera_switch:{
                updateCameraType();
                break;
            }
        }
    }

    private void updateOverlay(){
        if(mOverlayVisible) {
            overlay.setVisibility(View.VISIBLE);
            overlayToggle.setImageResource(R.drawable.ic_filter_none_white_24dp);
        }
        else {
            overlay.setVisibility(View.GONE);
            overlayToggle.setImageResource(R.drawable.ic_filter_white_24dp);
        }
    }

    private void updateFlash(){
        switch (controls.getFlashType()){
            case FLASH_AUTO:{
                controls.setFlashType(SlyCamera.FlashType.FLASH_OFF);
                flashToggle.setImageResource(R.drawable.ic_flash_off_white_24dp);
                break;
            }
            case FLASH_OFF:{
                controls.setFlashType(SlyCamera.FlashType.FLASH_ON);
                flashToggle.setImageResource(R.drawable.ic_flash_on_white_24dp);
                break;
            }
            case FLASH_ON:{
                controls.setFlashType(SlyCamera.FlashType.FLASH_AUTO);
                flashToggle.setImageResource(R.drawable.ic_flash_auto_white_24dp);
                break;
            }
        }
    }

    private void cameraAdjustments(){
        Toast.makeText(getActivity(), R.string.unavaliable, Toast.LENGTH_SHORT).show();
    }

    private void updateCameraType(){
        switch(controls.getCameraType()){
            case CAMERA_BACK:{
                controls.setCameraType(SlyCamera.CameraType.CAMERA_FRONT);
                switchToggle.setImageResource(R.drawable.ic_camera_rear_white_24dp);
                break;
            }
            case CAMERA_FRONT:{
                controls.setCameraType(SlyCamera.CameraType.CAMERA_BACK);
                switchToggle.setImageResource(R.drawable.ic_camera_front_white_24dp);
                break;
            }
        }
    }

    @Override
    public void onCameraRotated(int orientation) {
        if(mInitialized) rotateControls(orientation);
    }

    @Override
    public void onCameraCacheCleared(long sizeCleared, int nrOfFilesRemoved, boolean forcedClear) {
        Activity activity = getActivity();
        if(activity != null) {
            if (forcedClear)
                Toast.makeText(getActivity(), "Cache cleared, " + sizeCleared / 1000 + " MB" +
                        " removed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUnlockFocus() {

    }

    @Override
    public void onLockFocus() {

    }

    @Override
    public void onPreCapture() {

    }

    @Override
    public void onCaptureComplete(File imageFile) {
        Activity activity = getActivity();
        if(activity != null) {
            if (mPromptToSave) {
                DisplayFragment displayFragment = DisplayFragment.newInstance(imageFile, ID,
                        mSaveToGallery);
                displayFragment.show(getFragmentManager(), "DisplayFragment");
            } else {
                Intent intent = new Intent(activity, ImageSaveIntent.class);
                intent.putExtra(ImageSaveIntent.DELETE_TMP_IMAGE, true);
                intent.putExtra(ImageSaveIntent.IMAGE_ABSOLUTE_PATH, imageFile.getAbsolutePath());
                intent.putExtra(ImageSaveIntent.IMAGE_SAVE_TO_GALLERY, mSaveToGallery);
                intent.putExtra(ImageSaveIntent.PROJECT_ID, ID);
                activity.startService(intent);
            }
        }else {
            Log.e(TAG, " no activity ?");
        }
    }

    @Override
    public void onError(int code, String msg) {

    }
}
