package com.skaarstein.makinit.slycamera;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.skaarstein.makinit.services.ProjectSaverIntent;
import com.skaarstein.makinit.slycamera.SlyCameraPagerAdapter.*;
import com.skaarstein.makinit.R;
import com.skaarstein.makinit.utils.DatabaseHandler;
import com.skaarstein.makinit.utils.ImageResolution;
import com.skaarstein.makinit.utils.ImageUtils;
import com.skaarstein.makinit.views.GalleryFlipper;

/**
 * Created by Martin on 03.04.2016.
 */
public class OverlayFragment extends Fragment implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener {

    private static final String TAG = OverlayFragment.class.getName();

    private int ID;
    private int coverID;
    private int coverOpacity;
    private PagerControls pageControls;
    private GalleryAdapter galleryAdapter;
    private GalleryFlipper flipper;
    private SeekBar seekBar;
    private boolean initialized;

    public static OverlayFragment newInstance(@NonNull int ID, PagerControls pageControls){
        OverlayFragment overlayFragment = new OverlayFragment();
        overlayFragment.ID = ID;
        overlayFragment.pageControls = pageControls;
        return overlayFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera_overlay, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null || !initialized){
            flipper = (GalleryFlipper)view.findViewById(R.id.os_overlay_flipper);
            DatabaseHandler dh = new DatabaseHandler(getActivity());
            Integer[] ids = dh.getImageIDs(ID);

            if(ids == null) {
                initialized = false;
                return;
            }

            galleryAdapter = new GalleryAdapter(getActivity(), 0, ids);
            flipper.setAdapter(galleryAdapter);
            view.findViewById(R.id.os_close_camera).setOnClickListener(this);
            view.findViewById(R.id.os_open_camera).setOnClickListener(this);
            seekBar = (SeekBar) view.findViewById(R.id.os_opacity);
            seekBar.setOnSeekBarChangeListener(this);
            seekBar.setProgress(coverOpacity);
            initialized = true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.os_open_camera:{
                pageControls.setCurrentItem(1, true);
                break;
            }
            case R.id.os_close_camera:{
                pageControls.close();
                break;
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        flipper.setAlpha(progress/100f);
        coverOpacity = progress;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();

        if(activity != null) {
            SharedPreferences pref = getActivity()
                    .getSharedPreferences(String.valueOf(ID), Context.MODE_PRIVATE);
            coverOpacity = pref.getInt(SettingsFragment.PREF_COVER_OPACITY, 75);
            coverID = pref.getInt(SettingsFragment.PREF_COVER_ID, -1);

            if(seekBar != null) {
                seekBar.setProgress(coverOpacity);
                flipper.setSelection(galleryAdapter.getImagePosition(coverID));
                DatabaseHandler dh = new DatabaseHandler(activity);
                galleryAdapter.updateImages(dh.getImageIDs(ID));
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(galleryAdapter != null) {
            Activity activity = getActivity();
            int coverID = galleryAdapter.getImageId(flipper.getSelectedItemPosition());
            if (activity != null) {
                Intent intent = new Intent(activity, ProjectSaverIntent.class);
                intent.putExtra(ProjectSaverIntent.PROJECT_ID, ID);
                intent.putExtra(ProjectSaverIntent.PROJECT_COVER_ID, coverID);
                intent.putExtra(ProjectSaverIntent.NOTIFY, false);
                activity.startService(intent);
            }

            SharedPreferences.Editor editor = getActivity().getSharedPreferences(
                    String.valueOf(ID), Context.MODE_PRIVATE).edit();
            editor.putInt(SettingsFragment.PREF_COVER_ID, coverID);
            editor.putInt(SettingsFragment.PREF_COVER_OPACITY, coverOpacity);
            editor.commit();
        }
    }

    private class GalleryAdapter extends ArrayAdapter<Integer>{

        // Declare Variables
        Context context;
        Integer[] ids;
        DatabaseHandler dh;

        public GalleryAdapter(Context context, int resource, Integer[] objects) {
            super(context, resource, objects);
            this.context = context;
            updateImages(objects);
            this.dh = new DatabaseHandler(context);
        }

        public void updateImages(Integer[] objects){
            if(objects != null) this.ids = objects;
        }

        public int getImageId(int position){
            if(position < 0) return ids[ids.length -1];
            return ids[position];
        }

        public int getImagePosition(int id){
            for(int i = 0; i < ids.length; i++){
                if(ids[i] == id) return i;
            }
            return ids.length -1 ;
        }

        @Override
        public int getCount() {
            if(ids == null) {
                return 0;
            }
            return ids.length;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) convertView = LayoutInflater.from(
                    getActivity()).inflate(R.layout.camera_overlay_item, parent, false);

            ImageView imageView = (ImageView)convertView;
            byte[] data = dh.getImageData(ids[position]);
            imageView.setImageBitmap(ImageUtils.scaleImage(data, ImageResolution.P1080p));

            return convertView;
        }


    }
}
