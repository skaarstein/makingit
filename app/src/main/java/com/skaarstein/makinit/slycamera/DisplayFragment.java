package com.skaarstein.makinit.slycamera;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skaarstein.makinit.R;
import com.skaarstein.makinit.edit.ImageFragment;
import com.skaarstein.makinit.services.ImageSaveIntent;
import com.skaarstein.makinit.utils.ExifUtil;

import java.io.File;

public class DisplayFragment extends DialogFragment implements View.OnClickListener,
        ImageFragment.OnInfoUpdatedListener{

    private static final String TAG = DisplayFragment.class.getName();
    private ImageView imageDisplay;
    private int prjID;
    private File tempFile;
    private Bitmap tempImage;
    private String tmpTitle;
    private String tmpDesc;
    private static int FLING_TREESHOLD = 200;
    private boolean imageSaveToGallery;

    public static DisplayFragment newInstance(File tempFile, int prjID, boolean imageSaveToGallery){
        DisplayFragment display = new DisplayFragment();
        display.tempFile = tempFile;
        display.prjID = prjID;
        display.imageSaveToGallery = imageSaveToGallery;
        return display;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.Dialog_Display);

        int orientation = getResources().getConfiguration().orientation;
        switch (orientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageDisplay = (ImageView)view.findViewById(R.id.display_image);
        view.findViewById(R.id.display_accept).setOnClickListener(this);
        view.findViewById(R.id.display_discard).setOnClickListener(this);
        view.findViewById(R.id.display_edit_info).setOnClickListener(this);

        ImageLoader imageLoader = new ImageLoader();
        imageLoader.execute(tempFile);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_display_new, container, false);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.display_accept:
                Intent intent = new Intent(getActivity(), ImageSaveIntent.class);
                intent.putExtra(ImageSaveIntent.DELETE_TMP_IMAGE, true);
                intent.putExtra(ImageSaveIntent.IMAGE_ABSOLUTE_PATH, tempFile.getAbsolutePath());
                intent.putExtra(ImageSaveIntent.IMAGE_SAVE_TO_GALLERY, imageSaveToGallery);
                intent.putExtra(ImageSaveIntent.PROJECT_ID, prjID);
                intent.putExtra(ImageSaveIntent.IMAGE_DESCRIPTION, tmpDesc);
                intent.putExtra(ImageSaveIntent.IMAGE_TITLE, tmpTitle);
                getActivity().startService(intent);
                dismiss();
                break;
            case R.id.display_discard:
                if(tempFile != null) tempFile.delete();
                dismiss();
                break;
            case R.id.display_edit_info:{
                ImageFragment imageFragment = ImageFragment.newInstance(tempImage,
                        tmpTitle, tmpDesc, this);
                imageFragment.show(getFragmentManager(), "ImageInfoEdit");
                break;
            }
        }
    }

    @Override
    public void onInfoUpdated(String title, String description) {
        tmpDesc = description;
        tmpTitle = title;
    }

    @Override
    public void onInfoEditClose() {

    }

    private class ImageLoader extends AsyncTask<File, Integer, Bitmap>{

        @Override
        protected Bitmap doInBackground(File... params) {
            File inputFile = params[0];
            Bitmap tempBitmap = BitmapFactory.decodeFile(inputFile.getAbsolutePath());
            Bitmap finalBitmap = ExifUtil.rotateBitmap(inputFile.getAbsolutePath(),
                    tempBitmap);
            return finalBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            tempImage = bitmap;

            Matrix m = imageDisplay.getImageMatrix();
            imageDisplay.setScaleType(ImageView.ScaleType.MATRIX);
            RectF displayRect = new RectF(0, 0, imageDisplay.getWidth(), imageDisplay.getHeight());
            RectF imageRect = new RectF(0, 0, tempImage.getWidth(), tempImage.getHeight());
            m.setRectToRect(imageRect, displayRect, Matrix.ScaleToFit.FILL);
            imageDisplay.setImageBitmap(tempImage);
            imageDisplay.setImageMatrix(m);
        }
    }
}
